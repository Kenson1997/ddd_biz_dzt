import cn.hutool.core.util.StrUtil;
import com.hlsz.dzt.domain.dzt.entity.DztDescEntity;
import lombok.Data;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/5 13:08
 */

public class StringTest {

    @Test
    public void test01() {
        DztDescEntity dztDescEntity = new DztDescEntity("0");
        dztDescEntity.setDiameter("222");

        System.out.println(dztDescEntity.getDiameter());
        System.out.println(dztDescEntity.getDiaType());


    }


    @Test
    public void test02() {
        String s1 = "aabbccdd";
        String s2 = "aaddccbb";

        System.out.println(StrUtil.similar(s1, s2));

    }


    @Data
    class A {
        private String aaa;
    }

    @Data
    class B {
        private String bbb;
    }

    @Data
    class C {
        private String ccc;
    }

    @Data
    class D {
        private A a;
        private B b;
        private C c;
    }

    @Data
    class E {
        private String aaa;
        private String bbb;
        private String ccc;

        public E(String aaa, String bbb, String ccc) {
            this.aaa = aaa;
            this.bbb = bbb;
            this.ccc = ccc;
        }
    }

    @Test
    public void test003() {
        E e = new E("112", "ddf", "aac");
        D convert = convert(new D(), e,A.class,B.class,C.class);
        System.out.println(convert);
    }

    public <T, S> T convert(T t, S source, Class<?>... classes) {
        try {
            Class<?> targetClass = t.getClass();

            for (Class<?> clazz : classes) {
                Field[] targetFields = clazz.getDeclaredFields();
                for (Field targetField : targetFields) {
                    Field sourceField = source.getClass().getDeclaredField(targetField.getName());
                    sourceField.setAccessible(true);

                    Object sourceValue = sourceField.get(source);

                    Field targetObjField = targetClass.getDeclaredField(clazz.getSimpleName().toLowerCase());
                    targetObjField.setAccessible(true);

                    Object targetObj = targetObjField.get(t);
                    Field innerTargetField = targetObj.getClass().getDeclaredField(targetField.getName());
                    innerTargetField.setAccessible(true);
                    innerTargetField.set(targetObj, sourceValue);
                }
            }

            return t;
        } catch (Exception e) {
            throw new RuntimeException("转换失败", e);
        }
    }
}
