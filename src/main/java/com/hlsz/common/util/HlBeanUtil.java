package com.hlsz.common.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjUtil;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/6 18:53
 */

public class HlBeanUtil {


    /** Bean 合成，  将多个bean 合成到一个bean */
    public static <T> T beanSynthesis(T t,Object... objs){
        for(Object obj:objs){
            if(ObjUtil.isNull(obj)){continue;}
            BeanUtil.copyProperties(obj,t);
        }
        return t;
    }



}


