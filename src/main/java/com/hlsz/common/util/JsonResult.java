package com.hlsz.common.util;

import java.util.HashMap;
import java.util.Map;

public class JsonResult extends HashMap<String,Object> {

    private static final String CODE_TAG ="code";
    private static final String MSG_TAG ="msg";
    private static final String DATA_TAG ="data";
    private static final String OPT_TAG ="success";

    public JsonResult(){
        put(CODE_TAG,200).put(MSG_TAG,"操作成功").put(OPT_TAG,true);}

    public static JsonResult error(){
        return error(500,"");
    }
    public static JsonResult error(String msg) {
        return error(500, msg);
    }
//    public static JsonResult error(ExceptionStatus es) {
//        return error(es.getCode(), es.getMessage());
//    }

    public static JsonResult error(int code, String msg) {
        JsonResult r = new JsonResult();
        r.put(CODE_TAG, code);
        r.put(OPT_TAG, false);
        r.put(MSG_TAG, msg);
        return r;
    }

    public static JsonResult ok(String msg) {
        JsonResult r = new JsonResult();
        r.put(MSG_TAG, msg);
        return r;
    }
    public static JsonResult ok(Object data) {
        JsonResult r = new JsonResult();
        r.put(MSG_TAG, "操作成功！");
        r.put(DATA_TAG, data);
        return r;
    }
    public static JsonResult ok(String msg,Object data) {
        JsonResult r = new JsonResult();
        r.put(MSG_TAG, msg);
        r.put(DATA_TAG, data);
        return r;
    }

    public static JsonResult ok(Map<String, Object> map) {
        JsonResult r = new JsonResult();
        r.putAll(map);
        return r;
    }

    public static JsonResult ok() {
        return new JsonResult();
    }

    @Override
    public JsonResult put(String key, Object value) {
        super.put(key, value);
        return this;
    }


    public JsonResult addAll(Map map){
       super.putAll(map);
        return this;
    }
}
