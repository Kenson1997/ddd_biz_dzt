package com.hlsz.common.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hlsz.common.base.BaseConditionEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: zhboot
 * @description: 查询条件工具类 【需要优化】 处理重复字段值， 利用枚举来处理条件，用StreamAPI,
 * 需要加上默认查询条件（根据条件判断）
 * @author: Kenson
 * @create: 2021-05-26 15:01
 **/
public class SelectConditionUtil<T> {


    public static QueryWrapper parseWhereSql(List<BaseConditionEntity> conditions) {
        QueryWrapper<?> queryWrapper = new QueryWrapper();
        if (CollUtil.isEmpty(conditions)) {
            return queryWrapper;
        }

        List<String> sqlParts = conditions.stream().filter(x -> StrUtil.equals(x.getConnType(), "lastSql")).map(x -> x.getLasSqls()).flatMap(List::stream).collect(Collectors.toList());
        List<BaseConditionEntity> conds = conditions.stream().filter(x -> !StrUtil.equals(x.getConnType(), "lastSql")).collect(Collectors.toList());
        conds.add(new BaseConditionEntity("lastSql",sqlParts));
        // queryWrapper.eq("CO_ID",ResourceUtil.getCurrentCoId());
        for(BaseConditionEntity t:conditions){
            switch(t.getConnType()){
                case "eq": queryWrapper.eq(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "ne": queryWrapper.ne(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "like": queryWrapper.like(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "like_decode": queryWrapper.like(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "leftlike": queryWrapper.likeLeft(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "rightlike": queryWrapper.likeRight(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "notlike": queryWrapper.notLike(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "gt": queryWrapper.gt(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "lt": queryWrapper.lt(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "ge": queryWrapper.ge(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "le": queryWrapper.le(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "lastSql": queryWrapper.last(parseSql(t.getLasSqls()));break;
                case "orderByAsc":queryWrapper.orderByAsc(StrUtil.toUnderlineCase(t.getField()),t.getValue());break;
                case "notIn":queryWrapper.notIn(StrUtil.toUnderlineCase(t.getField()),t.getCoList());break;
                case "in": queryWrapper.in(StrUtil.toUnderlineCase(t.getField()),t.getValue().split(","));break;
                case "or": queryWrapper.or();break;
            }
        }
        return queryWrapper;
    }



    static String parseSql(List<String> sqls) {
        if (CollUtil.isEmpty(sqls)) {
            return "";
        }
        String join = String.join(" AND ", sqls);
       // join = KcStringUtil.handleDyValue2(join, BeanUtil.beanToMap(HResourceUtil.getCurrentUser()));
        return "AND (" + join + ")";
    }

    /**
     * if(b.indexOf("%") !=-1){queryWrapper.like(a,b);} // 如果值存在%，就是模糊查询
     * else if(b.indexOf("-")!=-1){queryWrapper.between(a,b.split("-")[0],b.split("-")[1]);}// 如果值存在- 就是两者之间
     * else if(b.indexOf("，")!=-1){queryWrapper.in(a,b.split(","));}// 如果值存在, 就是多个匹配
     * else{queryWrapper.eq(a,b);
     *
     * @param map
     * @return
     */
    public static QueryWrapper parseWhereSql(Map<String, String> map) {
        QueryWrapper queryWrapper = new QueryWrapper();
        map.forEach((a, b) -> {
            if (b.indexOf("%") != -1) {
                queryWrapper.like(a, b.replace("%", ""));
            } // 如果值存在%，就是模糊查询
            else if (b.indexOf("->") != -1) {
                queryWrapper.between(a, b.split("->")[0], b.split("->")[1]);
            }// 如果值存在- 就是两者之间
            else if (b.indexOf(",") != -1) {
                queryWrapper.in(a, b.split(","));
            }// 如果值存在, 就是多个匹配
            else {
                queryWrapper.eq(a, b);
            }
        });

        return queryWrapper;
    }


    // @Test
    public void test1() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "afd%");
        map.put("b", "fda,gfds,fdsa");
        map.put("c", "1-100");
        map.put("d", "dcs");
        QueryWrapper queryWrapper = parseWhereSql(map);
        System.out.println(queryWrapper);
    }


}

