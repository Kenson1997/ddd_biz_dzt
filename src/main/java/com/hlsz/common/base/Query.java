package com.hlsz.common.base;

import lombok.Data;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/12 17:08
 */

@Data
public class Query {

    private Integer current;  // 当前页
    private Integer size; // 每页大小

    private String ascs; // 顺序

    private String descs; // 倒序


}
