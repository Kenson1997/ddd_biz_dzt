package com.hlsz.common.base;/**
 * ClassName: BServicesImpl
 * Description:
 *
 * @author Kenson
 * @date 2023/2/27 15:22
 */

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: Kenson
 * @Description:  基础服务封装
 * @date: 2023/2/27 15:22
 */
public class BaseServicesImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M,T> implements BService<T> {
    // 日志记录对象
    public   final Logger log = LoggerFactory.getLogger(this.getClass());



}
