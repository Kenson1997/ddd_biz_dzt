package com.hlsz.common.base;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * ClassName: BService
 * Description:  基础Service
 *
 * @author Kenson
 * @date 2023/2/27 15:21
 *
 */
public interface BService  <T> extends IService<T> {




}
