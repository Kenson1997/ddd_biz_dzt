package com.hlsz.common.shared_kernel.em4;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * @Description 重量计算：管道重量、设备重量、总重量、安装点数、预制重量、安装重量
 * @Author kenson
 * @Date 2024/8/5 14:42
 */

@Data
public class WeightCalculate {

    private BigDecimal value;  //

    private String unit;  // 单位
    private String metaType; // 材料类型（管材、型材）
//    private Object weightCalculateRule;   //重量计算规则
    private BigDecimal[] nums ;

    /** 长度 * 宽度 * 数量 * 单重*/
    public  BigDecimal cal(BigDecimal length,BigDecimal width,BigDecimal num,BigDecimal sWeight){
        return new Calculate().cal(length,width,num,sWeight);
    }

    public BigDecimal cal(BigDecimal length,BigDecimal weight,BigDecimal sWeight){
        return new Calculate().cal(length,weight,sWeight);
    }

    private static class Calculate{
        /**
         * @Description  长 *  宽 * 高 * 单重
         * @Author kenson
         * @Date 2024/8/5 20:13
         */
        private BigDecimal cal(BigDecimal length,BigDecimal weight ,BigDecimal num, BigDecimal sWeight){
            return length.multiply(weight).multiply(num).multiply(sWeight);
        }

        /**
         * @Description 长 * 0.001  * 宽 * 0.001 * 单重
         * @Author kenson
         * @Date 2024/8/5 20:15
         */
        private BigDecimal cal(BigDecimal length,BigDecimal weight , BigDecimal sWeight){
            return length.multiply(weight).multiply(sWeight).multiply(BigDecimal.valueOf(0.001)).multiply(BigDecimal.valueOf(0.001));
        }
    }

    //public Calculate init(String unit,String metaType ){
    public static WeightCalculate init(){
        return new WeightCalculate();
//        switch (unit){
//            case "米":
//                    if(StrUtil.equals(metaType,"管材")){
//                        // 管材：长度 * 数量 * 宽度 * 单重
//                       this.value= Arrays.stream(nums).reduce(BigDecimal.ONE, BigDecimal::multiply);
//                    } else if (StrUtil.equals(metaType,"型材")) {
//                        // 型材：长度*0.001 * 宽度 * 0.001 * 单重
//
//                    }else{
//                        System.out.println("材料类型不能为空！");
//                    }
//
//
//                break;
//            case  "NB":
//                //  数量  *  单重
//
//
//                break;
//            case  "PC":
//                //  数量  *  单重
//
//
//                break;
//            default:
//                // 长度 * 数量 * 宽度 * 单重
//
//        }
    }

}
