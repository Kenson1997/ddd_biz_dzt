package com.hlsz.common.shared_kernel.em4.rules;

import java.math.BigDecimal;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/5 13:32
 */

public interface DiameterTypeRule {

    default String   diameterType(BigDecimal diameterLength){
        if (diameterLength.compareTo(BigDecimal.valueOf(1000.0)) > 0) {
           return "大管";
        } else {
           return "小管";
        }
    }

}
