package com.hlsz.common.shared_kernel.em4.enums;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/5 12:59
 */

public enum LengthUnit {
    MM("mm"),
    CM("cm"),
    DM("dm"),
    M("m");

    private String value;

    LengthUnit(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
