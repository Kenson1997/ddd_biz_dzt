package com.hlsz.common.shared_kernel.em4;

import com.hlsz.common.shared_kernel.em4.enums.LengthUnit;
import com.hlsz.common.shared_kernel.em4.rules.DiameterTypeRule;
import jdk.nashorn.internal.ir.annotations.Immutable;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description 直径
 * @Author kenson
 * @Date 2024/8/5 12:48
 */

import java.math.BigDecimal;
import java.util.function.Function;

/**
 * @Description 共享内核： 直径信息
 * @Author kenson
 * @Date 2024/8/5 14:42
 */
@Data
@Immutable
public final class Diameter {

    private BigDecimal diameterLength;  // 直径长度
    private String type ; // 类型（大管、小管）
    private LengthUnit unit = LengthUnit.MM; //单位（mm、cm、dm、m....）

    //Diameter 初始化
    public static Diameter init(String diameterLength) {
        Diameter diameter = new Diameter();
        try {
            diameter.setDiameterLength(new BigDecimal(diameterLength));
        } catch (NumberFormatException e) {
            // 处理输入的直径长度不是有效数字的情况
            throw new IllegalArgumentException("输入的直径长度不是有效的数字: " + diameterLength);
        }
        if (diameter.getDiameterLength().compareTo(BigDecimal.valueOf(1000.0)) > 0) {
            diameter.setType("大管");
        } else {
                diameter.setType("小管");
        }
        return diameter;
    }

    //Diameter 初始化
    public static Diameter init(String diameterLength,DiameterTypeRule diameterTypeRule){
        Diameter diameter = new Diameter();
        try {
            BigDecimal bigDecimal = new BigDecimal(diameterLength);
            diameter.setDiameterLength(bigDecimal);
            diameter.setType(diameterTypeRule.diameterType(bigDecimal));
        } catch (NumberFormatException e) {
            // 处理输入的直径长度不是有效数字的情况
            throw new IllegalArgumentException("输入的直径长度不是有效的数字: " + diameterLength);
        }

        return diameter;
    }

    //Diameter 初始化
    public static Diameter init(String diameterLength, Function<BigDecimal, String> typeDeterminer) {
        Diameter diameter = new Diameter();
        try {
            diameter.setDiameterLength(new BigDecimal(diameterLength));
        } catch (NumberFormatException e) {
            // 处理输入的直径长度不是有效数字的情况
            throw new IllegalArgumentException("输入的直径长度不是有效的数字: " + diameterLength);
        }
        diameter.setType(typeDeterminer.apply(diameter.getDiameterLength()));
        return diameter;
    }
}




