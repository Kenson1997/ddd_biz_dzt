package com.hlsz.common.shared_kernel.generic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjUtil;
import lombok.Data;

import java.util.Map;

/**
 * @Description 物项信息
 * @Author kenson
 * @Date 2024/8/9 7:58
 */

@Data
public class MateInfo {

    private String mateCode;
    private String mateName;  //物项名称
    private String grade;  //钢号
    private String matType;  //材质
    private String mateType;  //材质类型
    private String specs;  //规格
    private String thickness;  // 厚度
    private String unitQ;  //单位
    private String unitW;    //直径1
    private String dia1;  //壁厚1
    private String thick1;  //壁厚系列
    private String wallThick;  //直径2
    private String dia2;  //壁厚2
    private String thick2;  //壁厚系列2
    private String wallThick2;
    private String names;
    private String ppType;
    private String frc;
    private String comUnits;
    private String desces;
    private String size1;
    private String size2;
    private String preLevel;
    private String safeClass;
    private String stands;
    private String rvsCoe;


    public MateInfo(Object object){
        BeanUtil.copyProperties(object,this);
//        if(object instanceof Map){
//            //Map<String, Object> stringObjectMap = BeanUtil.beanToMap(object);
//
//        }
    }


}
