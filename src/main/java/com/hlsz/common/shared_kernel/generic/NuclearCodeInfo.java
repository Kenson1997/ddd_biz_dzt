package com.hlsz.common.shared_kernel.generic;

import lombok.Data;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/13 10:49
 */

@Data
public class NuclearCodeInfo {
    private String units;  //机组
    private String systems; //系统
    private String building; //厂房
    private String subSystem;  //子系统
    private String area;  //区域
    private String room;  //房间
    private String leveles;  //标高

}
