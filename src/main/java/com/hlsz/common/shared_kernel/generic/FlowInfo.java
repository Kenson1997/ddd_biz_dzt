package com.hlsz.common.shared_kernel.generic;

import lombok.Data;

/**
 * @Description 流程信息
 * @Author kenson
 * @Date 2024/8/13 10:46
 */

@Data
public class FlowInfo {
    private String confirmEmp;   // 审核人
    private String confirmDate;  // 审核日期

}
