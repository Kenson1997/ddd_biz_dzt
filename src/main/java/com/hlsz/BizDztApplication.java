package com.hlsz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.hlsz.**.dao","com.hlsz.**"})
public class BizDztApplication {
    public static void main(String[] args) {
        SpringApplication.run( BizDztApplication.class, args);
    }
}