package com.hlsz.dzt.message.converter;

import com.hlsz.common.shared_kernel.em4.WeightCalculate;
import com.hlsz.dzt.domain.dzt.entity.IsoMaterialEntity;
import com.hlsz.dzt.northbound.remote.message.DztpwResponse;
import com.hlsz.dzt.southbound.port.repository.IsoInfoRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description  等轴图点数计算装配器
 * @Author kenson
 * @Date 2024/8/6 14:37
 */

@Component
public class DztpwCalculateAssembler {

    @Resource
    private IsoInfoRepository isoInfoRepository;

    /**  根据等轴图id  获取等轴图的点数信息*/
    public DztpwResponse of(String id){
        DztpwResponse dztpwResponse = new DztpwResponse();
            // 材料总重
            BigDecimal materialSumWeight = new BigDecimal("0");
            // 特殊设备总重量
            BigDecimal specialEquipmentSumWeight = new BigDecimal("0");
            // 总阀门重量
            BigDecimal valveSumWeight = new BigDecimal("0");
            //  总在线部件重量
            BigDecimal onlineComponentsSumWeight = new BigDecimal("0");

         /*【材料数据】调用 南向网关的数据查询， 获取等轴图下所有的材料信息*/
            List<IsoMaterialEntity> materials =  isoInfoRepository.materiallist(id);
            for(IsoMaterialEntity isoMaterialEntity: materials){
                BigDecimal materialLength = new BigDecimal(isoMaterialEntity.getLength()); // 材料长度
                BigDecimal materialWidth =new BigDecimal(isoMaterialEntity.getWidth()); // 材料宽度
                BigDecimal materialNum =new BigDecimal(isoMaterialEntity.getAmount()); // 材料数量
                BigDecimal materialSWeight =new BigDecimal(isoMaterialEntity.getUnitW()); // 材料单重
                BigDecimal materialWeight = WeightCalculate.init().cal(materialLength, materialWidth, materialNum, materialSWeight);
                materialSumWeight = materialSumWeight.add(materialWeight);// 累加单个重量
            }

        /*【在线部件数据】 调用南向网关的数据数据查询，获取等轴图下所有的在线部件数据*/


        /*【特殊设备重量数据】 调用南向网关的数据查询， 获取等轴图下所有的特殊设备数据*/


        /*【阀门数据】 调用南向网关的数据查询，获取等轴图下所有的阀门数据*/



        // 预制重量 为 等轴图下材料总重量
        dztpwResponse.setPreWeight(materialSumWeight);
        //  总重量
        dztpwResponse.setTWeight(materialSumWeight.add(specialEquipmentSumWeight));
        // 安装重量
        dztpwResponse.setInsWeight(materialSumWeight.add(specialEquipmentSumWeight).add(valveSumWeight).add(onlineComponentsSumWeight));
        // 设备重量
        dztpwResponse.setEquiWeight(specialEquipmentSumWeight);
        // 管道重量
        dztpwResponse.setPipeWeight(materialSumWeight);

        return dztpwResponse;
    }

}
