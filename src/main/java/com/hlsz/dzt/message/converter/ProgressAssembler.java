package com.hlsz.dzt.message.converter;

import com.hlsz.common.base.BaseConditionEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description 进度指示书装配器
 * @Author kenson
 * @Date 2024/8/6 15:39
 */

@Component
public class ProgressAssembler {

    /** 获取进度指示书中的点数*/
    public BigDecimal get(List<BaseConditionEntity> conditions){

        return new BigDecimal("1.0001");
    }

}
