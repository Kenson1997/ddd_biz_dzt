package com.hlsz.dzt.domain.dzt.valueObject;

import lombok.Data;

/**
 * @Description 上下游信息
 * @Author kenson
 * @Date 2024/8/13 11:03
 */
@Data
public class UdStreamInfo {
    private String emp;  //工作包
    private String lineNo;  //管线号
    private String Units;  //机组
    private String dnSpool;  //等轴图/设备
    private String spool;  //管段
    private String matType;  //物项类型
    private String mateName;  // 物项名称
    private String mateCode;  //物项代码
    private String spec;  //  规格
    private String rccm;  //RCCM
    private String diam;  //直径
    private String thick;  //厚度
    private String thickSer;  //厚度系数
    private String steel;  //钢号
    private String material;  //材质
    private String wedPat;  //连接部件号
    private String blDraw;  //所在图
    private String pipeClass2;  //管道级别

    /**
     * @Description TODO
     * @Author kenson
     * @Params : emp,lineNo,Units,dnSpool,spool,matType,mateName,mateCode,spec,rccm,diam,thick,thickSer,steel,material,wedPat,blDraw,pipeClass2
     * @Date 2024/8/13 15:16
     */
    public UdStreamInfo(String emp, String lineNo, String Units, String dnSpool, String spool, String matType, String mateName, String mateCode, String spec, String rccm, String diam, String thick, String thickSer, String steel, String material, String wedPat, String blDraw, String pipeClass2){

    }



}



