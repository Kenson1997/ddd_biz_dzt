package com.hlsz.dzt.domain.dzt.entity;

import cn.hutool.core.bean.BeanUtil;
import com.hlsz.common.shared_kernel.generic.FlowInfo;
import com.hlsz.common.shared_kernel.generic.MateInfo;
import com.hlsz.common.shared_kernel.generic.NuclearCodeInfo;
import com.hlsz.dzt.domain.dzt.valueObject.UdStreamInfo;
import com.hlsz.dzt.southbound.adapter.repository.po.PipWeldsPo;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/13 10:39
 */

@Data
public class PipWeldsEntity {
    private String id;  //主键
    private String mainId;  //外键
    private FlowInfo flowInfo;  //流程信息
    @Getter
    @Setter
    private NuclearCodeInfo nuclearCodeInfo; // 核电编码
    @Getter
    @Setter
    private MateInfo mateInfo ;  // 物项信息
    @Getter
    @Setter
    private UdStreamInfo upMateInfo; // 上游信息
    @Getter
    @Setter
    private UdStreamInfo downMateInfo; // 下游信息
    private String serial;  //制造号
    private String assembly;  //焊缝号/接口标识
    private String newVer;  //最新版
    private String validYn;  //有效
    private String bendNum;  //弯曲数量
    private String weldType;  //焊接类型
    private String sequence;  //标准工序
    private String jointType;  //焊接方式
    private String rccm;  //规范等级
    private String mateGroup;  //材料组别
    private String pipCls;  //管道级别
    private String preCls;  //压力级别
    private String groupCode;  //组别号
    private String safeCls;  //安全级别
    private String weldInch;  //焊寸
    private String tempLink;  //临时连接
    private String jointReq;  //连接技术要求
    private String chkDta;  //验收标准
    private String rms;  //保留项
    private String serveChk;  //在役检查
    private String adjustYn;  //调节口
    private String weldingLine;  //丁字焊缝
    private String difficultyFlag;  //困难焊口
    private String difficultyDate;  //困难口标注日期
    private String deleteYn;  //删除标记
    private String pipeLineNo;  //管线号
    private String rtSamplingNo;  //不参与RT抽批
    private String pipeIso;  //等轴图号
    private String prefabErect;  //焊缝类型
    private String serveWeldMethods;  //在役焊缝检验方法
    private String rms2;  //保留类型
    private String zyqChk;  //是否役前检查
    private String preSite;  //预制地点
    private String ifWk;  //是否温控
    private String ifYumai;  //是否预埋
    private String formTiein;  //坡口形式
    private String ifBoss;  //是否BOSS头
    private String wedProcess;  //焊接工艺
    private String ifJxh;  //机械化焊
    private String ifRcl;  //热处理
    private String ifHskl;  //焊丝控Cr
    //厚度1、厚度2	上游厚度通过上游五项带出。下游是下游带出，对比取小的放到厚度1中，厚度2中的值是预制场用，现场不用
    private String thickness2;  //厚度2
    private String thickness1;  //厚度1

    public static PipWeldsEntity init(){
        return new PipWeldsEntity();
    }

    public List<PipWeldsEntity> initList(List<Object> objs){
        List<PipWeldsEntity> pipWeldsEntities = new ArrayList<>();
        for(Object obj : objs){
            Map<String, Object> stringObjectMap = BeanUtil.beanToMap(obj);
            PipWeldsEntity pipWelds = new PipWeldsEntity();
            BeanUtil.copyProperties(stringObjectMap,pipWelds);
           // pipWelds.setUpMateInfo();
        }

        return null;
    }

    public PipWeldsPo transPo() {
        PipWeldsPo po = new PipWeldsPo();

        return po;
    }


    public void setNuclearCodeInfo(NuclearCodeInfo nuclearCodeInfo) {
        this.nuclearCodeInfo = nuclearCodeInfo;
    }

    public void setMateInfo(Object object) {
        this.mateInfo =  new MateInfo(object);
    }

    public void setUpMateInfo(Object object) {

        this.upMateInfo = upMateInfo;
    }

    public void setDownMateInfo(Object object) {
        Map bean = BeanUtil.toBean(object, Map.class);
        UdStreamInfo udStreamInfo = new UdStreamInfo("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        this.downMateInfo = downMateInfo;
    }
}
