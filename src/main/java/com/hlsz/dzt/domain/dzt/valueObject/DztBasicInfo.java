package com.hlsz.dzt.domain.dzt.valueObject;

import lombok.Data;

/**
 * @Description 等轴图基本信息
 * @Author kenson
 * @Date 2024/8/5 12:11
 */
@Data
public class DztBasicInfo {
    private String isometric;  //等轴图号
    private String isoDrawNo;  //原图编码
    private String docid;  //图纸编号
    private String volLabel;  //卷标
    private String pages;  //页码
    private String mapsheet;  //图幅
    private String drawStatus;  //图纸状态
    private String drawType;  //图纸类型
    private String ver;  //版本
    private String newVer;  //最新版
    private String units;  //机组
    private String systems;  //系统
    private String subsystems;  //子系统
    private String building;  //厂房
    private String area;  //区域
    private String room;  //房间
    private String clevel;  //标高
    private String cleve2;  //顶标高



}
