package com.hlsz.dzt.domain.dzt.entity;

import com.hlsz.common.shared_kernel.em4.WeightCalculate;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @Description 等轴图点数重量信息
 * @Author kenson
 * @Date 2024/8/5 14:38
 */

@Data
public class DztpwEntity {

    private String id;
    @Getter
    @Setter
    private WeightCalculate pointType;   // 点数计算分类  --- 套用点数计算规则
    private BigDecimal pipeWeight;   // 管道重量(kg)
    private BigDecimal equiWeight;   // 设备重量(kg)
    private BigDecimal tWeight;   // 总重量(kg)
    private BigDecimal tDots;   // 总点数
    private BigDecimal preWeight;   // 预制重量(kg)
    private BigDecimal preDots;   // 预制点数
    private BigDecimal insWeight;   // 安装重量(kg)
    private BigDecimal insDots;   // 安装点数


    public WeightCalculate getPointType() {
        return pointType;
    }

    public void setPointType(WeightCalculate pointType) {
        this.pointType = pointType;
    }
}
