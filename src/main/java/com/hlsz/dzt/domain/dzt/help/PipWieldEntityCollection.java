package com.hlsz.dzt.domain.dzt.help;

import com.hlsz.dzt.domain.dzt.entity.PipWeldsEntity;
import com.hlsz.dzt.northbound.remote.message.PipWeldRequest;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/14 9:26
 */
@Data
public class PipWieldEntityCollection {
    private List<PipWeldsEntity>  pipWelds;


    public PipWieldEntityCollection(List<PipWeldRequest> pipWeldRequests){

    }
}
