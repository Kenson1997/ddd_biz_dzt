package com.hlsz.dzt.domain.dzt.entity;

import lombok.Data;

/**
 * @Description 管段信息
 * @Author kenson
 * @Date 2024/8/6 9:23
 */
@Data
public class PipInfoEntity {

    private String id;
    private String spool;  //管段号
    private String mateCode;  //物项代码
    private String mateCodeB;  //备用物项代码
    private String matType;  //材质
    private String room;  //房间
    private String unitQ;  //单位
    private String length;  //长度
    private String amount;  //数量
    private String qaClass;  //QA级别
    private String rccm;  //核级
    private String unitW;  //单重
    private String weight;  //重量
    private String thick1;  //壁厚1
    private String thick2;  //壁厚2
    private String bendAngle1;  //弯曲角度1
    private String preCls;  //压力等级
    private String pipLineNo;  //管线号
    private String systems;  //系统
    private String dia1;  //直径1
    private String dia2;  //直径2
    private String grade;  //钢号
    private String macType;  //制造类型
    private String safeClass;  //安全级别
    private String fcr;  //管段fcr
    private String ifHp;  //是否保温
    private String hpType;  //保温类型
    private String hpThickness;  //保温厚度
    private String hpLength;  //保温长度
    private String ifBr;  //是否伴热
    private String phLength;  //伴热长度
    private String ifBend;  //弯管


}
