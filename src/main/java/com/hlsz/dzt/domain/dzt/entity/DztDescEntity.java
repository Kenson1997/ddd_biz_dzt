package com.hlsz.dzt.domain.dzt.entity;

import com.hlsz.common.shared_kernel.em4.Diameter;
import com.hlsz.common.shared_kernel.em4.rules.DiameterTypeRule;
import com.hlsz.common.shared_kernel.em4.rules.impl.DiameterTypeRuleImpl;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @Description 等轴图描述信息
 * @Author kenson
 * @Date 2024/8/5 12:16
 */

@Data
public class DztDescEntity {

    private String id;
    @Getter
    @Setter
    private Diameter diameter ;  //直径
    private String diaType;  //直径类型
    private String grade;  //钢号
    private String matType;  //材质
    private String rccm;  //RCCM级别
    private String pipeGrade;  //管道等级
    private String sysMedia;  //介质
    private String pipCls;  //管道级别
    private String qaClass;  //质保级别
    private String dustClass;  //清洁度级别
    private String bendStd;  //弯管标准
    private String safeCls;  //安全等级
    private String earthquake;  //抗震类别
    private String compuNo;  //应力计算书号
    private String ifHp;  //是否保温
    private String hpType;  //保温类型
    private String hpLength;  //保温长度
    private String hpThickness;  //保温厚度
    private String workH;  //工作温度(℃)
    private String workP;  //工作压力(MPa)
    private String ifPaint;  //是否油漆
    private String paintSys;  //涂层系统
    private String paintCol;  //油漆颜色
    private String ifBr;  //是否伴热
    private String phLength;  //伴热长度
    private String upDrawCode;   //上游图号
    private String downDrawCode;   //下游图号

    void  transPo(){};



    public DztDescEntity(String decimal){
        this.diameter = Diameter.init(decimal);
    }

    public void setDiameter(String length) {
        // 初始化 Diameter 方式一
        //this.diameter = Diameter.init(length);
        // 初始化 Diameter 方式二
        //this.diameter = Diameter.init(length, new DiameterTypeRuleImpl());
        this.diameter =   Diameter.init(length, diameterLength -> {
            if (diameterLength.compareTo(BigDecimal.valueOf(1000)) > 0) {
                return "大管";
            } else {
                return "小管";
            }
        });
        this.diaType =diameter.getType();
    }

    public Double getDiameter() {
        return this.diameter.getDiameterLength().doubleValue();
    }
}
