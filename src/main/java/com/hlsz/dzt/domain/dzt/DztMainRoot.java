package com.hlsz.dzt.domain.dzt;

import cn.hutool.core.bean.BeanUtil;
import com.hlsz.common.util.HlBeanUtil;
import com.hlsz.dzt.domain.dzt.entity.*;
import com.hlsz.dzt.domain.dzt.valueObject.DztBasicInfo;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoInfoVo;
import com.hlsz.dzt.northbound.remote.message.IsoInfoRequest;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/5 12:10
 */

@Data
public class DztMainRoot {

    // 图纸基本信息
    private DztBasicInfo dztBasicInfo ;

    // 图纸描述信息
    private DztDescEntity dztDescEntity ;

    // 等轴图 点数计算信息
    private DztpwEntity dztpwEntity ;

    // 文件信息
    private FileDescEntity fileDescEntity ;

    // 图纸材料信息
    private List<IsoMaterialEntity> isoMaterials;

    // 管段信息
    private List<PipInfoEntity> pipInfos;

    //  焊口信息
    private List<PipWeldsEntity> pipWelds;



/**  初始化 材料集合*/
    public  List<IsoMaterialEntity> initMaterials(){
        this.isoMaterials = new ArrayList<>();
        return this.isoMaterials;
    }

/** 初始化点数计算信息*/
    public DztMainRoot initDztpwEntity(Object obj){
        DztpwEntity  dztpwEntityCopy = new DztpwEntity();
        BeanUtil.copyProperties(obj,dztpwEntityCopy);
        this.dztpwEntity = dztpwEntityCopy;
        return this;
    }

    /** 图纸描述信息*/
    public DztMainRoot  initDztDescEntity(Object obj){
        DztDescEntity  dztDescEntityCopy = new DztDescEntity(BeanUtil.toBean(obj, IsoInfoRequest.class).getDiameter());
        BeanUtil.copyProperties(obj,dztDescEntityCopy);

        this.dztDescEntity =dztDescEntityCopy;
        return this;
    }

    /** 初始化文件信息*/
    public DztMainRoot initFileDescEntity(Object obj){
        FileDescEntity  fileDescEntityCopy = new FileDescEntity();
        BeanUtil.copyProperties(obj,fileDescEntityCopy);
        this.fileDescEntity =fileDescEntityCopy;
        return this;
    }

/** 初始化图纸基本信息*/
    public DztMainRoot initDztBasicInfo(Object obj){
        DztBasicInfo  dztBasicInfoCopy = new DztBasicInfo();
        BeanUtil.copyProperties(obj,dztBasicInfoCopy);
        this.dztBasicInfo =dztBasicInfoCopy;
        return this;
    }


    /** 初始化管段信息*/
    public DztMainRoot initPipInfoList(List<Object> objs){
        List<PipInfoEntity> pipinfosCopy = BeanUtil.copyToList(objs, PipInfoEntity.class);
        this.pipInfos =pipinfosCopy;
        return this;
    }




    /** 初始化图纸材料信息*/
    public DztMainRoot initIsoMaterialList(List<Object> objs){
        List<IsoMaterialEntity> isoMateriallist = BeanUtil.copyToList(objs, IsoMaterialEntity.class);
        this.isoMaterials =isoMateriallist;
        return this;
    }


    /** 初始化焊口信息*/
    public DztMainRoot initPipWeldLists(List<Object> objs){
        DztMainRoot dztMainRoot = new DztMainRoot();

        return dztMainRoot;
    }


    /**  聚合根 转换成 IsoInfoPo*/
    public IsoInfoPo transPo(){
        IsoInfoPo synthesis = HlBeanUtil.beanSynthesis(new IsoInfoPo(), this.dztBasicInfo, this.dztDescEntity);
        return synthesis;
    }





}
