package com.hlsz.dzt.domain.dzt.entity;

import lombok.Data;

/**
 * @Description 文件信息
 * @Author kenson
 * @Date 2024/8/5 14:47
 */

@Data
public class FileDescEntity {

    private String id;
    private String verModifyCode;   //版本修订码
    private String ifFu;   //FORUSE
    private String letterNo;   //FORUSE信函
    private String preLetterNo;   //预制信函号
    private String drawCancel;   //图纸取消
    private String drawCancelNotes;   //取消原因
    private String fileDistribute;   //文件分发单
    private String delIdcDate;   //移交IDC
    private String distDate;   //分发日期
    private String executeDesc;   //执行状态描述

}
