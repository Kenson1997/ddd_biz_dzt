package com.hlsz.dzt.domain.dzt.entity;

import cn.hutool.core.bean.BeanUtil;
import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;
import lombok.Data;

/**
 * @Description 等轴图材料信息
 * @Author kenson
 * @Date 2024/8/6 9:08
 */

@Data
public class IsoMaterialEntity {

   private String id;
   private String mainId;
   private String sitCode;
   private String  isometric;  // 等轴图号
   private String   docid;  // 图纸编号
   private String  pipLineNo;  // 管道号
   private String   mancType;  // 制造类型
   private String  unitArea;  // 单位面积
   private String   matSize;  // 尺寸
   private String  spool;  // 管段号
   private String   mateHolder;  // 物权属性
   private String  partsNo;  // 部件号
   private String   mateCode;  // 物项代码
   private String  mateCodeB;  // 备用物项代码
   private String   mateName;  // 物项名称
   private String  grade;  // 钢号
   private String   matType;  // 材质
   private String  mateType;  // 材质类型
   private String   specs;  // 规格
   private String  unitQ;  // 单位
   private String   length;  // 长度
   private String  width;  // 宽度
   private String   amount;  // 数量
   private String  qaClass;  // QA级别
   private String   rccm;  // 核级
   private String  pipCls;  // 管道级别
   private String   preCls;  // 压力级别
   private String  unitD;  // 单位(设计)
   private String   amountD;  // 数量(设计)
   private String  unitW;  // 单重
   private String   weight;  // 重量
   private String  weightD;  // 重量(设计)
   private String   priAmount;  // 原始材料数
   private String  fcrAmount;  // FCR数
   private String   dia1;  // 直径1
   private String  thick1;  // 壁厚1
   private String   wallThick;  // 壁厚系列
   private String  dia2;  // 直径2
   private String   thick2;  // 壁厚2
   private String  wallThick2;  // 壁厚系列2
   private String   preWas;  // 预制WAS号
   private String  preWasDate;  // 预制WAS接收
   private String   wasNo;  // 安装WAS号
   private String  insWasDate;  // 安装WAS日期
   private String   ifPre;  // 是否预制
   private String  onlineParts;  // 在线部件


   public MaterialPo transPo(){
      MaterialPo materialPo = new MaterialPo();
      BeanUtil.copyProperties(this,materialPo);
      return materialPo;
   }

}
