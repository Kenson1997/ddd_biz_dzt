package com.hlsz.dzt.domain.dzt.valueObject;

import lombok.Data;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/9 18:10
 */
@Data
public class PipLineInfo {

    private String pipLineNo;   //管线号
    private String units;   //机组
    private String systems;   //系统
    private String pipCls;   //管道级别
    private String diameter;   //管径


}




