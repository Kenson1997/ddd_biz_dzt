package com.hlsz.dzt.domain.domain_service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hlsz.dzt.domain.dzt.DztMainRoot;
import com.hlsz.dzt.domain.dzt.entity.IsoMaterialEntity;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;
import com.hlsz.dzt.southbound.port.repository.IsoInfoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description 等轴图领域服务
 * @Author kenson
 * @Date 2024/8/6 18:33
 */
@Service
public class IsoDomainService {

    @Resource
    private IsoInfoRepository isoInfoRepository;

    /** 等轴图 新增*/
    public boolean save(DztMainRoot dztMainRootForAdd) {
        IsoInfoPo isoInfoPo = dztMainRootForAdd.transPo();
        boolean save = isoInfoRepository.save(isoInfoPo);
        return save;
    }
    /** 等轴图 更新*/
    public boolean update(DztMainRoot dztMainRootForUpdate) {
        IsoInfoPo isoInfoPo = dztMainRootForUpdate.transPo();
        boolean save = isoInfoRepository.updateById(isoInfoPo);
        return save;
    }


    public int isoAddMaterials(DztMainRoot materialAdd) {
        List<IsoMaterialEntity> isoMaterials = materialAdd.getIsoMaterials();
        int i =isoInfoRepository.saveBatchMaterials(isoMaterials);
        return i;
    }

    public int isoChangeMaterials(DztMainRoot materialChange) {
        List<IsoMaterialEntity> isoMaterials = materialChange.getIsoMaterials();
        int i = isoInfoRepository.changeBatchMaterials(isoMaterials);
        return 0;
    }


    public boolean delete(String id) {
        boolean b = isoInfoRepository.removeById(id);
        return b;
    }

    public IPage<IsoInfoPo> pageList(Page page, QueryWrapper queryWrapper) {
        Page lists = isoInfoRepository.page(page,queryWrapper);
        return lists;
    }

    /**  等轴图审核  ①修改等轴图表的审核信息， ②各子表数据计算 并更新子表的审核信息*/
    @Transactional(rollbackFor = Exception.class)
    public boolean isoApprove(String id) {
        //  要转换成各个PO 执行保存

        return false;
    }

    public IPage<MaterialPo> materialList(Page page, String id) {
      IPage<MaterialPo> listPage =  isoInfoRepository.materialPageList(page,id);
      return listPage;
    }

    public int materialRemove(List<String> ids) {
        int flag = isoInfoRepository.materialRemove(ids);
        return flag;
    }

    public int materialRemove2(DztMainRoot materialAdd) {
        int num  = isoInfoRepository.materialRemove2(materialAdd);
        return num;
    }
}
