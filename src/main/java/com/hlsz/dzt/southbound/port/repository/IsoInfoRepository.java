package com.hlsz.dzt.southbound.port.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hlsz.common.base.BService;
import com.hlsz.dzt.domain.dzt.DztMainRoot;
import com.hlsz.dzt.domain.dzt.entity.IsoMaterialEntity;
import com.hlsz.dzt.domain.dzt.entity.PipInfoEntity;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;

import java.util.List;

public interface IsoInfoRepository extends BService<IsoInfoPo> {


    /** 获取材料列表*/
    List<IsoMaterialEntity> materiallist(String id);

    IPage<MaterialPo> materialPageList(Page page , String id);


    /** 获取管段信息列表*/
    List<PipInfoEntity> pipInfos(String id);

    int saveBatchMaterials(List<IsoMaterialEntity> isoMaterials);

    int changeBatchMaterials(List<IsoMaterialEntity> isoMaterials);

    int materialRemove(List<String> ids);

    int materialRemove2(DztMainRoot materialAdd);
}
