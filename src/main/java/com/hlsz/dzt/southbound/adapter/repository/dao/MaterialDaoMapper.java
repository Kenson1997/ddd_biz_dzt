package com.hlsz.dzt.southbound.adapter.repository.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MaterialDaoMapper  extends BaseMapper<MaterialPo> {
}
