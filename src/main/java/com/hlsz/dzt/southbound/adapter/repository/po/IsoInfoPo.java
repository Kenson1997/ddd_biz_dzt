package com.hlsz.dzt.southbound.adapter.repository.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hlsz.common.util.BaseEntity;
import lombok.Data;

import java.time.LocalTime;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/6 15:04
 */

@TableName("PIP_ISO_DRAW")
@Data
public class IsoInfoPo extends BaseEntity {

    @TableId(type =IdType.ASSIGN_ID)
    private String id;

    @TableField("UNIQUE_CODE")
    private String uniqueCode;

    @TableField("ISOMETRIC")
    private String isometric;

    @TableField("DOCID")
    private String docid;

    @TableField("ISO_DRAW_NO")
    private String isoDrawNo;

    @TableField("VOL_LABEL")
    private String volLabel;

    @TableField("PAGES")
    private String pages;

    @TableField("MAPSHEET")
    private String mapsheet;

    @TableField("DRAW_STATUS")
    private String drawStatus;

    @TableField("DRAW_TYPE")
    private String drawType;

    @TableField("VER")
    private String ver;

    @TableField("NEW_VER")
    private String newVer;

    @TableField("UNITS")
    private String units;

    @TableField("SYSTEMS")
    private String systems;

    @TableField("SUBSYSTEMS")
    private String subsystems;

    @TableField("BUILDING")
    private String building;

    @TableField("AREA")
    private String area;

    @TableField("ROOM")
    private String room;

    @TableField("CLEVEL")
    private String clevel;

    @TableField("CLEVE2")
    private String cleve2;

    @TableField("DIA_TYPE")
    private String diaType;

    @TableField("GRADE")
    private String grade;

    @TableField("MAT_TYPE")
    private String matType;

    @TableField("RCCM")
    private String rccm;

    @TableField("PIPE_GRADE")
    private String pipeGrade;

    @TableField("SYS_MEDIA")
    private String sysMedia;

    @TableField("PIP_CLS")
    private String pipCls;

    @TableField("QA_CLASS")
    private String qaClass;

    @TableField("DUST_CLASS")
    private String dustClass;

    @TableField("BEND_STD")
    private String bendStd;

    @TableField("SAFE_CLS")
    private String safeCls;

    @TableField("EARTHQUAKE")
    private String earthquake;

    @TableField("COMPU_NO")
    private String compuNo;

    @TableField("IF_HP")
    private String ifHp;

    @TableField("HP_TYPE")
    private String hpType;

    @TableField("HP_LENGTH")
    private String hpLength;

    @TableField("HP_THICKNESS")
    private String hpThickness;

    @TableField("WORK_H")
    private String workH;

    @TableField("WORK_P")
    private String workP;

    @TableField("IF_PAINT")
    private String ifPaint;

    @TableField("PAINT_SYS")
    private String paintSys;

    @TableField("PAINT_COL")
    private String paintCol;

    @TableField("IF_BR")
    private String ifBr;

    @TableField("PH_LENGTH")
    private String phLength;

    @TableField("PRE_LENGTH")
    private String preLength;

    @TableField("INS_LENGTH")
    private String insLength;

    @TableField("PRJ_LENGTH")
    private String prjLength;

    @TableField("POINT_TYPE")
    private String pointType;

    @TableField("PIPE_WEIGHT")
    private String pipeWeight;

    @TableField("EQUI_WEIGHT")
    private String equiWeight;

    @TableField("T_WEIGHT")
    private String tWeight;

    @TableField("T_DOTS")
    private String tDots;

    @TableField("PRE_WEIGHT")
    private String preWeight;

    @TableField("PRE_DOTS")
    private String preDots;

    @TableField("INS_WEIGHT")
    private String insWeight;

    @TableField("INS_DOTS")
    private String insDots;

    @TableField("DES_COMP")
    private String desComp;

    @TableField("PRE_COMP")
    private String preComp;

    @TableField("INS_COMP")
    private String insComp;

    @TableField("BUILD_DRAWING")
    private String buildDrawing;

    @TableField("BULID_ISSUE_DATE")
    private LocalTime bulidIssueDate;

    @TableField("UP_DRAW_CODE")
    private String upDrawCode;

    @TableField("DOWN_DRAW_CODE")
    private String downDrawCode;

    @TableField("VER_MODI_CODE")
    private String verModiCode;

    @TableField("IF_FU")
    private String ifFu;

    @TableField("LETTER_NO")
    private String letterNo;

    @TableField("PRE_LETTER_NO")
    private String preLetterNo;

    @TableField("DRAW_CANCEL")
    private String drawCancel;

    @TableField("DRAW_CANCEL_NOTES")
    private String drawCancelNotes;

    @TableField("FILE_DISTRIB")
    private String fileDistrib;

    @TableField("DEL_IDC_DATE")
    private LocalTime delIdcDate;

    @TableField("DIST_DATE")
    private LocalTime distDate;

    @TableField("EXECUTE_DESC")
    private String executeDesc;

    @TableField("PACT_NO")
    private String pactNo;

    @TableField("PAINT_AREA")
    private String paintArea;

    @TableField("INS_DRAW_FLAG")
    private String insDrawFlag;

    @TableField("DOC_CODE")
    private String docCode;

    @TableField("TYPICAL_DRAW")
    private String typicalDraw;

    @TableField("TYPICAL_DRAW_VER")
    private String typicalDrawVer;

    @TableField("SIT_CODE")
    private String sitCode;

    @TableField("IF_PRE")
    private String ifPre;

    @TableField("IF_CONVERSED")
    private String ifConversed;

    @TableField("IF_CONVERSED_FINISH")
    private String ifConversedFinish;

    @TableField("IF_YUMAI")
    private String ifYumai;

    @TableField("EMP_PRE")
    private String empPre;

    @TableField("EMP_INS")
    private String empIns;

    @TableField("STATUS_DATE")
    private LocalTime statusDate;

    @TableField("WBS_CODE_PRE")
    private String wbsCodePre;

    @TableField("WBS_CODE_INS")
    private String wbsCodeIns;

    @TableField("CONFIRM_EMP")
    private String confirmEmp;

    @TableField("CONFIRM_DATE")
    private LocalTime confirmDate;

    @TableField("DRAW_INCMAN_PRE")
    private String drawIncmanPre;

    @TableField("DRAW_INCDATE_PRE")
    private LocalTime drawIncdatePre;

    @TableField("WF_PRE")
    private String wfPre;

    @TableField("WF_VER_PRE")
    private String wfVerPre;

    @TableField("WF_INCDATE_PRE")
    private LocalTime wfIncdatePre;

    @TableField("WF_CANCEL_PRE")
    private String wfCancelPre;

    @TableField("WF_CCDATE_PRE")
    private LocalTime wfCcdatePre;

    @TableField("WF_CSDATE_PRE")
    private LocalTime wfCsdatePre;

    @TableField("ISSUE_PRE")
    private String issuePre;

    @TableField("ISSUE_BY_PRE")
    private String issueByPre;

    @TableField("ISSUE_DATE_PRE")
    private LocalTime issueDatePre;

    @TableField("ISUSE_VER_PRE")
    private String isuseVerPre;

    @TableField("ISUSE_STATUS_PRE")
    private String isuseStatusPre;

    @TableField("TQP_CODE_PRE")
    private String tqpCodePre;

    @TableField("WAS_RECEIVER")
    private LocalTime wasReceiver;

    @TableField("IS_MAT")
    private String isMat;

    @TableField("IS_SEND")
    private String isSend;

    @TableField("SEND_DEPT")
    private String sendDept;

    @TableField("SEND_DATE")
    private LocalTime sendDate;

    @TableField("ISS_DATE")
    private LocalTime issDate;

    @TableField("MAT_ISS")
    private LocalTime matIss;

    @TableField("PRE_COM_DATE")
    private LocalTime preComDate;

    @TableField("RT_DATE")
    private LocalTime rtDate;

    @TableField("CLOSE_BATCH")
    private String closeBatch;

    @TableField("BATCH_DATE")
    private LocalTime batchDate;

    @TableField("PAINT_DATE")
    private LocalTime paintDate;

    @TableField("XD_DATE")
    private LocalTime xdDate;

    @TableField("QC_DATE")
    private LocalTime qcDate;

    @TableField("PRE_EMR_DATE")
    private LocalTime preEmrDate;

    @TableField("INSTORE_DATE")
    private LocalTime instoreDate;

    @TableField("STORE_AREA")
    private String storeArea;

    @TableField("FACE_STORE")
    private String faceStore;

    @TableField("OUTSTORE_DATE")
    private LocalTime outstoreDate;

    @TableField("MOVE_MAN")
    private String moveMan;

    @TableField("CAR_NO")
    private String carNo;

    @TableField("INS_CAR_NO")
    private String insCarNo;

    @TableField("DRAW_INCMAN_INS")
    private String drawIncmanIns;

    @TableField("DRAW_INCDATE_INS")
    private LocalTime drawIncdateIns;

    @TableField("WF_INS")
    private String wfIns;

    @TableField("WF_VER_INS")
    private String wfVerIns;

    @TableField("ISSUE_INS")
    private String issueIns;

    @TableField("ISSUE_BY_INS")
    private String issueByIns;

    @TableField("ISSUE_DATE_INS")
    private LocalTime issueDateIns;

    @TableField("ISUSE_VER_INS")
    private String isuseVerIns;

    @TableField("ISUSE_STATUS_INS")
    private String isuseStatusIns;

    @TableField("TQP_CODE_INS")
    private String tqpCodeIns;



    @TableField("DELETE_YN")
    private String deleteYn;

    @TableField("DELETE_TIME")
    private LocalTime deleteTime;



    @TableField("FF_AREA")
    private String ffArea;

    @TableField("PT_CONSIGN")
    private String ptConsign;

    @TableField("PT_CONSIGN_CODE")
    private String ptConsignCode;

    @TableField("PT_CONSIGN_EMP")
    private String ptConsignEmp;

    @TableField("PT_CONSIGN_DATE")
    private LocalTime ptConsignDate;

    @TableField("TM_CHECK")
    private String tmCheck;

    @TableField("TM_CHECK_CODE")
    private String tmCheckCode;

    @TableField("TM_CHECK_EMP")
    private String tmCheckEmp;

    @TableField("TM_CHECK_DATE")
    private LocalTime tmCheckDate;

    @TableField("IM_HD")
    private String imHd;

    @TableField("IM_HD_CODE")
    private String imHdCode;

    @TableField("IM_HD_EMP")
    private String imHdEmp;

    @TableField("IM_HD_DATE")
    private LocalTime imHdDate;

    @TableField("PAINT_TYPE")
    private String paintType;

    @TableField("SL_CODE")
    private String slCode;

    @TableField("SL_MAN")
    private String slMan;

    @TableField("SL_DATE")
    private LocalTime slDate;

    @TableField("CK_CODE")
    private String ckCode;

    @TableField("CK_MAN")
    private String ckMan;

    @TableField("CK_DATE")
    private LocalTime ckDate;

    @TableField("INSTOR")
    private String instor;

    @TableField("INSTOR_EMP")
    private String instorEmp;

    @TableField("STORE_DATE")
    private LocalTime storeDate;

    @TableField("FU_RECEIVER")
    private LocalTime fuReceiver;

    @TableField("MODULE")
    private String module;

    @TableField("PENETRATE")
    private String penetrate;

    @TableField("EMP_NO")
    private String empNo;

    @TableField("DIA")
    private String dia;

    @TableField("SYPHON_STD")
    private String syphonStd;

    @TableField("KEEPARCH")
    private String keeparch;

    @TableField("WJLX")
    private String wjlx;

    @TableField("DISPENSE_DATE")
    private LocalTime dispenseDate;

    @TableField("INCEPT")
    private String incept;

    @TableField("LETTER_DATE")
    private LocalTime letterDate;

    @TableField("WF")
    private String wf;

    @TableField("WF_VER")
    private String wfVer;

    @TableField("WF_CODE_INS")
    private String wfCodeIns;

    @TableField("SPOOLINE")
    private String spooline;

    @TableField("DIAMETER")
    private String diameter;

    @TableField("DRAW_NO")
    private String drawNo;

    @TableField("OLD_ID")
    private String oldId;

    @TableField("PLAN_SDATE_WELD")
    private LocalTime planSdateWeld;

    @TableField("PLAN_EDATE_WELD")
    private LocalTime planEdateWeld;

    @TableField("FACT_SDATE_WELD")
    private LocalTime factSdateWeld;

    @TableField("FACT_EDATE_WELD")
    private LocalTime factEdateWeld;

    @TableField("ONE_WEEK_WELD")
    private String oneWeekWeld;

    @TableField("TWO_WEEK_WELD")
    private String twoWeekWeld;

    @TableField("SGBZ_WELD")
    private String sgbzWeld;

    @TableField("SGBZ_DATE_WELD")
    private LocalTime sgbzDateWeld;

    @TableField("PLAN_SDATE_PAIN")
    private LocalTime planSdatePain;

    @TableField("PLAN_EDATE_PAIN")
    private LocalTime planEdatePain;

    @TableField("FACT_SDATE_PAIN")
    private LocalTime factSdatePain;

    @TableField("FACT_EDATE_PAIN")
    private LocalTime factEdatePain;

    @TableField("ONE_WEEK_PAIN")
    private String oneWeekPain;

    @TableField("TWO_WEEK_PAIN")
    private String twoWeekPain;

    @TableField("SGBZ_PAIN")
    private String sgbzPain;

    @TableField("SGBZ_DATE_PAIN")
    private LocalTime sgbzDatePain;

    @TableField("QADP_DATE_PRE")
    private LocalTime qadpDatePre;

    @TableField("PT_DATE")
    private LocalTime ptDate;

    @TableField("PT_CLOSE_BATCH")
    private String ptCloseBatch;

    @TableField("TQP_CODE_PRE_VER")
    private String tqpCodePreVer;

    @TableField("TQP_CODE_INS_VER")
    private String tqpCodeInsVer;

    @TableField("PLAN_EDIT_MAN_WELD")
    private String planEditManWeld;

    @TableField("PLAN_EDIT_DATE_WELD")
    private LocalTime planEditDateWeld;

    @TableField("PLAN_EDIT_MAN_PAIN")
    private String planEditManPain;

    @TableField("PLAN_EDIT_DATE_PAIN")
    private LocalTime planEditDatePain;

    @TableField("TECH_NAME_WELD")
    private String techNameWeld;

    @TableField("TECH_DATE_WELD")
    private LocalTime techDateWeld;

    @TableField("QC1_NAME_WELD")
    private String qc1NameWeld;

    @TableField("QC1_DATE_WELD")
    private LocalTime qc1DateWeld;

    @TableField("QC1_BACK_REASON_WELD")
    private String qc1BackReasonWeld;

    @TableField("QC2_NAME_WELD")
    private String qc2NameWeld;

    @TableField("QC2_DATE_WELD")
    private LocalTime qc2DateWeld;

    @TableField("QC2_BACK_REASON_WELD")
    private String qc2BackReasonWeld;

    @TableField("TECH_NAME_PAIN")
    private String techNamePain;

    @TableField("TECH_DATE_PAIN")
    private LocalTime techDatePain;

    @TableField("QC1_NAME_PAIN")
    private String qc1NamePain;

    @TableField("QC1_DATE_PAIN")
    private LocalTime qc1DatePain;

    @TableField("QC1_BACK_REASON_PAIN")
    private String qc1BackReasonPain;

    @TableField("QC2_NAME_PAIN")
    private String qc2NamePain;

    @TableField("QC2_DATE_PAIN")
    private LocalTime qc2DatePain;

    @TableField("QC2_BACK_REASON_PAIN")
    private String qc2BackReasonPain;

    @TableField("PRE_FILE_CODE")
    private String preFileCode;

    @TableField("IF_BORDER")
    private String ifBorder;

    @TableField("APROVAL_DATE")
    private LocalTime aprovalDate;

    @TableField("APROVAL_REPLY_DATE")
    private LocalTime aprovalReplyDate;

    @TableField("DOC_NAME")
    private String docName;

    @TableField("INTERCODE")
    private String intercode;

    @TableField("REC_CODE")
    private String recCode;

    @TableField("BACK_COLOR")
    private String backColor;

    @TableField("BACK_WIDTH")
    private String backWidth;

    @TableField("DIST_COLOR")
    private String distColor;

    @TableField("DIST_WIDTH")
    private String distWidth;

    @TableField("STATE_COLOR")
    private String stateColor;

    @TableField("STATE_WIDTH")
    private String stateWidth;

    @TableField("SD_ARROW_SPEC")
    private String sdArrowSpec;

    @TableField("LON_RAD_SIGN")
    private String lonRadSign;

    @TableField("INS_PLAN_DATE")
    private LocalTime insPlanDate;

    @TableField("DIST_Q")
    private String distQ;

    @TableField("FIVE_PASS_FLAG")
    private String fivePassFlag;

    @TableField("FIVE_PLAN_PASS")
    private LocalTime fivePlanPass;

    @TableField("FIVE_PLAN_DATE")
    private LocalTime fivePlanDate;

    @TableField("FIVE_PLAN_MON")
    private String fivePlanMon;

    @TableField("WAS_COND_Y_N")
    private String wasCondYN;

    @TableField("MATE_COND_Y_N")
    private String mateCondYN;

    @TableField("FIVE_PLAN_Y_N")
    private String fivePlanYN;

    @TableField("FLOORS")
    private String floors;

    @TableField("OPER_P")
    private String operP;

    @TableField("REMARKS")
    private String remarks;

    @TableField("PLAN_DATE")
    private LocalTime planDate;

    @TableField("PR_LEV")
    private String prLev;

    @TableField("OPENITEM_NO")
    private String openitemNo;

    @TableField("RT_DATE_FP")
    private LocalTime rtDateFp;

    @TableField("VFP_BAK_BZ")
    private String vfpBakBz;

    @TableField("BS_SHR")
    private String bsShr;

    @TableField("BS_SHRQ")
    private LocalTime bsShrq;

    @TableField("PRE_LETTER_DATE")
    private LocalTime preLetterDate;

    @TableField("RCCM2")
    private String rccm2;

    @TableField("MOTION_H")
    private String motionH;

    @TableField("GFDJ")
    private String gfdj;

    @TableField("DUTY_ENGINEER")
    private String dutyEngineer;

    @TableField("QUOTA_COMPUTE_CODE")
    private String quotaComputeCode;

    @TableField("NNSA_CK")
    private String nnsaCk;

    @TableField("SS_SX_SGBZ")
    private String ssSxSgbz;

    @TableField("SS_SX_DATE")
    private LocalTime ssSxDate;

    @TableField("QUOTA_DATE")
    private LocalTime quotaDate;

    @TableField("PRE_REPORT_NO")
    private String preReportNo;

    @TableField("QADP_REPORT_NO")
    private String qadpReportNo;

    @TableField("TEAM_LEADER_NAME_WELD")
    private String teamLeaderNameWeld;

    @TableField("TEAM_LEADER_NAME_PAIN")
    private String teamLeaderNamePain;

    @TableField("WELD_EYE_RPT_CODE")
    private String weldEyeRptCode;

    @TableField("EPC_BUILDING")
    private String epcBuilding;

    @TableField("EPC_CLEVEL")
    private String epcClevel;

    @TableField("FHJC_NO")
    private String fhjcNo;

    @TableField("RTINS_SIGNNO")
    private String rtinsSignno;

    @TableField("RTPRE_SIGNNO")
    private String rtpreSignno;

    @TableField("DRAW_NO_NAME")
    private String drawNoName;

    @TableField("DRAW_NO_VER")
    private String drawNoVer;

    @TableField("CHECK_DATE")
    private LocalTime checkDate;

    @TableField("CHECK_MAN")
    private String checkMan;

    @TableField("RECORD_NO")
    private String recordNo;

    @TableField("SPECIAL_FF")
    private String specialFf;

    @TableField("IF_OUTDOOR")
    private String ifOutdoor;


}






