package com.hlsz.dzt.southbound.adapter.repository.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hlsz.dzt.southbound.adapter.repository.po.PipWeldsPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/13 10:38
 */

@Mapper
public interface PipWeldsDaoMapper extends BaseMapper<PipWeldsPo> {
}
