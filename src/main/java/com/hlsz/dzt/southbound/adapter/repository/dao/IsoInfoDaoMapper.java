package com.hlsz.dzt.southbound.adapter.repository.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/6 15:04
 */

@Mapper
public interface IsoInfoDaoMapper extends BaseMapper<IsoInfoPo> {
}
