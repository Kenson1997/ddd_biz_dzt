package com.hlsz.dzt.southbound.adapter.repository.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hlsz.common.util.BaseEntity;
import lombok.Data;

import java.time.LocalDateTime;

@TableName("PIP_WELDS")
@Data
public class PipWeldsPo extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @TableField("MAIN_ID")
    private String mainId;
    @TableField("ROOM")
    private String room;
    @TableField("SYSTEMS")
    private String systems;
    @TableField("SUBSYSTEMS")
    private String subsystems;
    @TableField("LEVELES")
    private String leveles;
    @TableField("SERIAL")
    private String serial;
    @TableField("ASSEMBLY")
    private String assembly;
    @TableField("NEW_VER")
    private String newVer;
    @TableField("VALID_YN")
    private String validYn;
    @TableField("BEND_NUM")
    private String bendNum;
    @TableField("WELD_TYPE")
    private String weldType;
    @TableField("SEQUENCE")
    private String sequence;
    @TableField("JOINT_TYPE")
    private String jointType;
    @TableField("RCCM")
    private String rccm;
    @TableField("MATE_GROUP")
    private String mateGroup;
    @TableField("PIP_CLS")
    private String pipCls;
    @TableField("PRE_CLS")
    private String preCls;
    @TableField("GROUP_CODE")
    private String groupCode;
    @TableField("SAFE_CLS")
    private String safeCls;
    @TableField("GRADE")
    private String grade;
    @TableField("MAT_TYPE")
    private String matType;
    @TableField("DIAMETER")
    private String diameter;
    @TableField("THICKNESS")
    private String thickness;
    @TableField("WELD_INCH")
    private String weldInch;
    @TableField("PAR_REQ")
    private String parReq;
    @TableField("M_VALUE")
    private String mValue;
    @TableField("CUSH_LRCM")
    private String cushLrcm;
    @TableField("SA_SPEC")
    private String saSpec;
    @TableField("SA_SIZE")
    private String saSize;
    @TableField("TEMP_LINK")
    private String tempLink;
    @TableField("TEMP_CUSH")
    private String tempCush;
    @TableField("JOINT_REQ")
    private String jointReq;
    @TableField("CHK_DTA")
    private String chkDta;
    @TableField("MAT_TYPE_UP")
    private String matTypeUp;
    @TableField("EMP_UP")
    private String empUp;
    @TableField("LINE_NO_UP")
    private String lineNoUp;
    @TableField("UP_UNITS")
    private String upUnits;
    @TableField("UP_SPOOL")
    private String upSpool;
    @TableField("SPOOL_UP")
    private String spoolUp;
    @TableField("RCCM_UP")
    private String rccmUp;
    @TableField("DIAM_UP")
    private String diamUp;
    @TableField("THICK_UP")
    private String thickUp;
    @TableField("THICK_SER_UP")
    private String thickSerUp;
    @TableField("WED_PAT_UP")
    private String wedPatUp;
    @TableField("LRCM_UP")
    private String lrcmUp;
    @TableField("STEEL_UP")
    private String steelUp;
    @TableField("MATERIAL_UP")
    private String materialUp;
    @TableField("BL_DRAW_UP")
    private String blDrawUp;
    @TableField("PIPE_CLASS1")
    private String pipeClass1;
    @TableField("MAT_TYPE_DOWN")
    private String matTypeDown;
    @TableField("EMP_DOWN")
    private String empDown;
    @TableField("LINE_NO_DOWN")
    private String lineNoDown;
    @TableField("DN_UNITS")
    private String dnUnits;
    @TableField("DN_SPOOL")
    private String dnSpool;
    @TableField("SPOOL_DOWN")
    private String spoolDown;
    @TableField("RCCM_DOWN")
    private String rccmDown;
    @TableField("DIAM_DOWN")
    private String diamDown;
    @TableField("THICK_DOWN")
    private String thickDown;
    @TableField("THICK_SER_DOWN")
    private String thickSerDown;
    @TableField("WED_PAT_DOWN")
    private String wedPatDown;
    @TableField("LRCM_DOWN")
    private String lrcmDown;
    @TableField("STEEL_DOWN")
    private String steelDown;
    @TableField("MATERIAL_DOWN")
    private String materialDown;
    @TableField("BL_DRAW_DOWN")
    private String blDrawDown;
    @TableField("PIPE_CLASS2")
    private String pipeClass2;
    @TableField("BIZHI")
    private String bizhi;
    @TableField("IS_JIAQ")
    private String isJiaq;
    @TableField("COLD_TEST")
    private String coldTest;
    @TableField("RMS_CLOSE_DATE")
    private LocalDateTime rmsCloseDate;
    @TableField("TFD_CODE")
    private String tfdCode;
    @TableField("FORM_CODE")
    private String formCode;
    @TableField("RMS")
    private String rms;
    @TableField("HY_AFTER")
    private String hyAfter;
    @TableField("POSTFLUSH")
    private String postflush;
    @TableField("FCR")
    private String fcr;
    @TableField("SERVE_CHK")
    private String serveChk;
    @TableField("ADJUST_YN")
    private String adjustYn;
    @TableField("WELDING_LINE")
    private String weldingLine;
    @TableField("DIFFICULTY_FLAG")
    private String difficultyFlag;
    @TableField("DIFFICULTY_DATE")
    private LocalDateTime difficultyDate;
    @TableField("CONFIRM_EMP")
    private String confirmEmp;
    @TableField("CONFIRM_DATE")
    private String confirmDate;
    @TableField("ISSUE")
    private String issue;
    @TableField("ISSUE_BY")
    private String issueBy;
    @TableField("ISSUE_DATE")
    private String issueDate;
    @TableField("VER")
    private String ver;
    @TableField("CONTROL_LIST_NO")
    private String controlListNo;
    @TableField("QUALITY_ITEM")
    private String qualityItem;
    @TableField("TQP")
    private String tqp;
    @TableField("TQP_VER")
    private String tqpVer;
    @TableField("DELETE_EMP")
    private String deleteEmp;
    @TableField("DELETE_CAUSE")
    private String deleteCause;
    @TableField("DELETE_DATE")
    private LocalDateTime deleteDate;
    @TableField("PRINT_WELD")
    private String printWeld;
    @TableField("PRINT_REMARK")
    private String printRemark;
    @TableField("PRINT_NUM")
    private String printNum;
    @TableField("PRINT_EMP")
    private String printEmp;
    @TableField("PRINT_DATE")
    private LocalDateTime printDate;
    @TableField("INPUT_COMP_EMP")
    private String inputCompEmp;
    @TableField("INPUT_COMP_DATE")
    private String inputCompDate;
    @TableField("INPUT_DEPT")
    private String inputDept;
    @TableField("WPQ")
    private String wpq;
    @TableField("WELD_POSI")
    private String weldPosi;
    @TableField("WELD_PROCESS")
    private String weldProcess;
    @TableField("WELD_FINISH")
    private String weldFinish;
    @TableField("ZC_EYE_CHEK")
    private String zcEyeChek;
    @TableField("EYEBALLING")
    private String eyeballing;
    @TableField("SOMA_BATCH_NO")
    private String somaBatchNo;
    @TableField("TO_BATCH")
    private String toBatch;
    @TableField("BATCHNO")
    private String batchno;
    @TableField("BATCH_RT")
    private String batchRt;
    @TableField("RT_DATE")
    private String rtDate;
    @TableField("PT_TO_BATCH")
    private String ptToBatch;
    @TableField("PT_BATCHNO")
    private String ptBatchno;
    @TableField("BATCH_PT")
    private String batchPt;
    @TableField("BEPAINT_DATE")
    private String bepaintDate;
    @TableField("UT_TO_BATCH")
    private String utToBatch;
    @TableField("UT_BATCHNO")
    private String utBatchno;
    @TableField("BATCH_UT")
    private String batchUt;
    @TableField("OVERSOUND_DATE")
    private String oversoundDate;
    @TableField("REP_YN")
    private String repYn;
    @TableField("ISO_PASS")
    private String isoPass;
    @TableField("JG_DOC")
    private String jgDoc;
    @TableField("JG_VER")
    private String jgVer;
    @TableField("JG_FUTU")
    private String jgFutu;
    @TableField("JG_NAME")
    private String jgName;
    @TableField("JG_TYPE")
    private String jgType;
    @TableField("SPECS")
    private String specs;
    @TableField("JG_JIANS")
    private String jgJians;
    @TableField("WATER_DISSOLVE")
    private String waterDissolve;
    @TableField("WA_PA_DATE")
    private String waPaDate;
    @TableField("FN_BATCHNO")
    private String fnBatchno;
    @TableField("WELD_SIGN")
    private String weldSign;
    @TableField("IF_REPAIR")
    private String ifRepair;
    @TableField("BUG_SERIAL")
    private String bugSerial;
    @TableField("ADJ_TIME")
    private String adjTime;
    @TableField("IF_JXH")
    private String ifJxh;
    @TableField("IF_RCL")
    private String ifRcl;
    @TableField("IF_DGK")
    private String ifDgk;
    @TableField("IF_HSKL")
    private String ifHskl;
    @TableField("QC_CODE")
    private String qcCode;
    @TableField("VTM")
    private String vtm;
    @TableField("ADJUST_TIME")
    private String adjustTime;
    @TableField("PRE_PAINT_DATE")
    private String prePaintDate;
    @TableField("ILL_MAT")
    private String illMat;
    @TableField("REFER_FILE")
    private String referFile;
    @TableField("REMARK")
    private String remark;
    @TableField("ANGLE")
    private String angle;
    @TableField("IS_ASS")
    private String isAss;
    @TableField("MAT_REQ_DATE")
    private String matReqDate;
    @TableField("ASS_DATE")
    private String assDate;
    @TableField("ASS_REMARK")
    private String assRemark;
    @TableField("ASS_TYPE")
    private String assType;
    @TableField("ERROR_REMARK")
    private String errorRemark;
    @TableField("DLFW")
    private String dlfw;
    @TableField("DYFW")
    private String dyfw;
    @TableField("HJSDFW")
    private String hjsdfw;
    @TableField("ZDXNL")
    private String zdxnl;
    @TableField("FORM_TIEIN")
    private String formTiein;
    @TableField("CHECK_PARTS")
    private String checkParts;
    @TableField("MAKE_WAY")
    private String makeWay;

    @TableField("DELETE_YN")
    private String deleteYn;
    @TableField("DELETE_TIME")
    private String deleteTime;

    @TableField("BATCH_PASS")
    private String batchPass;
    @TableField("PT_BATCH_PASS")
    private String ptBatchPass;
    @TableField("UT_BATCH_PASS")
    private String utBatchPass;
    @TableField("STATUS_WELD")
    private String statusWeld;
    @TableField("BATCHNUM")
    private String batchnum;
    @TableField("BATCHNUM_PT")
    private String batchnumPt;
    @TableField("BATCHNUM_UT")
    private String batchnumUt;
    @TableField("QC_MAN")
    private String qcMan;
    @TableField("QC_DATE")
    private String qcDate;
    @TableField("WBS_CODE")
    private String wbsCode;
    @TableField("RT_SAMPLING_NO")
    private String rtSamplingNo;
    @TableField("PIPE_LINE_NO")
    private String pipeLineNo;
    @TableField("AREA")
    private String area;
    @TableField("WED_PACK_TTL")
    private String wedPackTtl;
    @TableField("BUILDING")
    private String building;
    @TableField("PIPE_ISO")
    private String pipeIso;
    @TableField("PREFAB_ERECT")
    private String prefabErect;
    @TableField("SERVE_WELD_METHODS")
    private String serveWeldMethods;
    @TableField("TQP_REV")
    private String tqpRev;
    @TableField("TAKE_STD")
    private String takeStd;
    @TableField("SUP_WELD")
    private String supWeld;
    @TableField("CARATE_WHO")
    private String carateWho;
    @TableField("CARATE_DATE")
    private String carateDate;
    @TableField("LAST_CHK")
    private String lastChk;
    @TableField("RMS2")
    private String rms2;
    @TableField("SPEC_UP")
    private String specUp;
    @TableField("SPEC_DOWN")
    private String specDown;
    @TableField("ZYQ_CHK")
    private String zyqChk;
    @TableField("OVERSOUND")
    private String oversound;
    @TableField("PRE_SITE")
    private String preSite;
    @TableField("MATENAME_UP")
    private String matenameUp;
    @TableField("MATENAME_DOWN")
    private String matenameDown;
    @TableField("WPQ_CONF_DATE")
    private LocalDateTime wpqConfDate;
    @TableField("WPQ_CONF_MAN")
    private String wpqConfMan;
    @TableField("THICKNESS_2")
    private String thickness2;
    @TableField("HFSX_NO")
    private String hfsxNo;
    @TableField("PT_PERCENT")
    private String ptPercent;
    @TableField("RT_PERCENT")
    private String rtPercent;
    @TableField("BATCH_MT")
    private String batchMt;
    @TableField("MT_TO_BATCH")
    private String mtToBatch;
    @TableField("MT_BATCHNO")
    private String mtBatchno;
    @TableField("BATCHNUM_MT")
    private String batchnumMt;
    @TableField("MT_BATCH_PASS")
    private String mtBatchPass;
    @TableField("VISUAL_CHECKER")
    private String visualChecker;
    @TableField("VISUAL_DATE")
    private String visualDate;
    @TableField("SUPER_CHECKER")
    private String superChecker;
    @TableField("PLAN_SDATE_WELD")
    private String planSdateWeld;
    @TableField("PLAN_EDATE_WELD")
    private String planEdateWeld;
    @TableField("FACT_SDATE_WELD")
    private String factSdateWeld;
    @TableField("FACT_EDATE_WELD")
    private String factEdateWeld;
    @TableField("ONE_WEEK_WELD")
    private String oneWeekWeld;
    @TableField("TWO_WEEK_WELD")
    private String twoWeekWeld;
    @TableField("SGBZ_WELD")
    private String sgbzWeld;
    @TableField("SGBZ_DATE_WELD")
    private String sgbzDateWeld;
    @TableField("TECH_NAME_WELD")
    private String techNameWeld;
    @TableField("TECH_DATE_WELD")
    private String techDateWeld;
    @TableField("QC1_NAME_WELD")
    private String qc1NameWeld;
    @TableField("QC1_DATE_WELD")
    private String qc1DateWeld;
    @TableField("QC1_BACK_REASON_WELD")
    private String qc1BackReasonWeld;
    @TableField("QC2_NAME_WELD")
    private String qc2NameWeld;
    @TableField("QC2_DATE_WELD")
    private String qc2DateWeld;
    @TableField("QC2_BACK_REASON_WELD")
    private String qc2BackReasonWeld;
    @TableField("HY_DATE")
    private String hyDate;
    @TableField("PLAN_EDIT_MAN_WELD")
    private String planEditManWeld;
    @TableField("PLAN_EDIT_DATE_WELD")
    private String planEditDateWeld;
    @TableField("PLAN_EDIT_MAN_PAIN")
    private String planEditManPain;
    @TableField("PLAN_EDIT_DATE_PAIN")
    private String planEditDatePain;
    @TableField("PLAN_SDATE_PAIN")
    private String planSdatePain;
    @TableField("PLAN_EDATE_PAIN")
    private String planEdatePain;
    @TableField("FACT_SDATE_PAIN")
    private String factSdatePain;
    @TableField("FACT_EDATE_PAIN")
    private String factEdatePain;
    @TableField("ONE_WEEK_PAIN")
    private String oneWeekPain;
    @TableField("TWO_WEEK_PAIN")
    private String twoWeekPain;
    @TableField("SGBZ_PAIN")
    private String sgbzPain;
    @TableField("SGBZ_DATE_PAIN")
    private String sgbzDatePain;
    @TableField("TECH_NAME_PAIN")
    private String techNamePain;
    @TableField("TECH_DATE_PAIN")
    private String techDatePain;
    @TableField("QC1_NAME_PAIN")
    private String qc1NamePain;
    @TableField("QC1_DATE_PAIN")
    private String qc1DatePain;
    @TableField("QC1_BACK_REASON_PAIN")
    private String qc1BackReasonPain;
    @TableField("QC2_NAME_PAIN")
    private String qc2NamePain;
    @TableField("QC2_DATE_PAIN")
    private String qc2DatePain;
    @TableField("QC2_BACK_REASON_PAIN")
    private String qc2BackReasonPain;
    @TableField("INS_EMR_DATE")
    private String insEmrDate;
    @TableField("EYE_RPT_CODE")
    private String eyeRptCode;
    @TableField("TFD_REMARK")
    private String tfdRemark;
    @TableField("RT_REPLACE_REASON")
    private String rtReplaceReason;
    @TableField("PT_REPLACE_REASON")
    private String ptReplaceReason;
    @TableField("WELD_SGBZ")
    private String weldSgbz;
    @TableField("WELD_OVER_MAN")
    private String weldOverMan;
    @TableField("WELD_OVER_DATE")
    private String weldOverDate;
    @TableField("FIVE_PASS_FLAG")
    private String fivePassFlag;
    @TableField("FIVE_PLAN_PASS")
    private String fivePlanPass;
    @TableField("FIVE_PLAN_DATE")
    private String fivePlanDate;
    @TableField("FIVE_PLAN_MON")
    private String fivePlanMon;
    @TableField("RES_REASON")
    private String resReason;
    @TableField("RES_REMAK")
    private String resRemak;
    @TableField("FIVE_PLAN_Y_N")
    private String fivePlanYN;
    @TableField("WELDING_AMOUNT")
    private String weldingAmount;
    @TableField("JJG")
    private String jjg;
    @TableField("DM")
    private String dm;
    @TableField("YSZT")
    private String yszt;
    @TableField("WGQX")
    private String wgqx;
    @TableField("JL")
    private String jl;
    @TableField("EYE_RPT_REMARK")
    private String eyeRptRemark;
    @TableField("LAST_OPERATOR")
    private String lastOperator;
    @TableField("MOSTER_ACCEPT_DATE")
    private String mosterAcceptDate;
    @TableField("BLX")
    private String blx;
    @TableField("P_ANALYST")
    private String pAnalyst;
    @TableField("ANALYSIS_DATE")
    private LocalDateTime analysisDate;
    @TableField("SUPER_YN")
    private String superYn;
    @TableField("JCFF")
    private String jcff;
    @TableField("JCFF_REMARK")
    private String jcffRemark;
    @TableField("SXWT_NO")
    private String sxwtNo;
    @TableField("ZG_ANGLE")
    private String zgAngle;
    @TableField("FN_BATCHNO2")
    private String fnBatchno2;
    @TableField("FN_BATCHNO3")
    private String fnBatchno3;
    @TableField("SPEC")
    private String spec;
    @TableField("SPEC2")
    private String spec2;
    @TableField("SPEC3")
    private String spec3;
    @TableField("MAT_TC")
    private String matTc;
    @TableField("MAT_TC2")
    private String matTc2;
    @TableField("MAT_TC3")
    private String matTc3;
    @TableField("WELDING_NUM")
    private String weldingNum;
    @TableField("RT_SIGN_REMARK")
    private String rtSignRemark;
    @TableField("IF_CIRCUIT_IN")
    private String ifCircuitIn;
    @TableField("TEAM_LEADER_NAME")
    private String teamLeaderName;
    @TableField("ANGLE_HETA")
    private String angleHeta;
    @TableField("LENGTH_L")
    private String lengthL;
    @TableField("MATERIAL_TYPE")
    private String materialType;
    @TableField("WELD_REPORT_NO")
    private String weldReportNo;
    @TableField("RT_REPORT_NO")
    private String rtReportNo;
    @TableField("HEATINPUT")
    private String heatinput;
    @TableField("IF_RELEASE")
    private String ifRelease;
    @TableField("H_INPUT_YN")
    private String hInputYn;
    @TableField("RT_REPLACE_CODE")
    private String rtReplaceCode;
    @TableField("RT_REPLACE_APPROVER")
    private String rtReplaceApprover;
    @TableField("IF_WK")
    private String ifWk;
    @TableField("CONSTRUCTION")
    private String construction;
    @TableField("REMARKS")
    private String remarks;
    @TableField("UP_EMP")
    private String upEmp;
    @TableField("UP_DATE")
    private String upDat;
    @TableField("REPAIR_DIG_NO")
    private String repairDigNo;
    @TableField("PT_SAMPLING_NO")
    private String ptSamplingNo;
    @TableField("ALL_PT")
    private String allPt;
    @TableField("ALL_RT")
    private String allRt;
    @TableField("HY_BORDER")
    private String hyBorder;
    @TableField("BEFORE_CHK")
    private String beforeChk;
    @TableField("UT_PERCENT")
    private String utPercent;
    @TableField("IF_YUMAI")
    private String ifYumai;
    @TableField("WELD_CLASS")
    private String weldClass;
    @TableField("RCP")
    private String rcp;
    @TableField("PRINT_LOCK")
    private String printLock;
    @TableField("BATCH_DATE")
    private String batchDate;
    @TableField("BATCH_DATE_PT")
    private String batchDatePt;
    @TableField("BATCH_DATE_UT")
    private String batchDateUt;
}