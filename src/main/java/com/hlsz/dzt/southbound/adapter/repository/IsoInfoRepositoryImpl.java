package com.hlsz.dzt.southbound.adapter.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hlsz.common.base.BaseServicesImpl;
import com.hlsz.dzt.domain.dzt.DztMainRoot;
import com.hlsz.dzt.domain.dzt.entity.IsoMaterialEntity;
import com.hlsz.dzt.domain.dzt.entity.PipInfoEntity;
import com.hlsz.dzt.southbound.adapter.repository.dao.IsoInfoDaoMapper;
import com.hlsz.dzt.southbound.adapter.repository.dao.MaterialDaoMapper;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;
import com.hlsz.dzt.southbound.port.repository.IsoInfoRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/6 15:04
 */

@Service
public class IsoInfoRepositoryImpl extends BaseServicesImpl<IsoInfoDaoMapper,IsoInfoPo> implements IsoInfoRepository {

    @Resource
    private MaterialDaoMapper materialDaoMapper;

    @Override
    public List<IsoMaterialEntity> materiallist(String id) {
        return null;
    }

    @Override
    public IPage<MaterialPo> materialPageList(Page page, String id) {
        Page materialLists = materialDaoMapper.selectPage(page, new QueryWrapper<MaterialPo>().eq("MAIN_ID", id));
        return materialLists;
    }

    @Override
    public List<PipInfoEntity> pipInfos(String id) {
        return null;
    }

    @Override
    public int saveBatchMaterials(List<IsoMaterialEntity> isoMaterials) {
        int insert = 0;
        for(IsoMaterialEntity isoMaterialEntity : isoMaterials){
            MaterialPo materialPo = isoMaterialEntity.transPo();
            insert += materialDaoMapper.insert(materialPo);
        }
        return insert;
    }

    @Override
    public int changeBatchMaterials(List<IsoMaterialEntity> isoMaterials) {
        int change = 0;
        for(IsoMaterialEntity isoMaterialEntity : isoMaterials){
            MaterialPo materialPo = isoMaterialEntity.transPo();
            change += materialDaoMapper.updateById(materialPo);
        }
        return change;
    }

    @Override
    public int materialRemove(List<String> ids) {
        int i = materialDaoMapper.deleteBatchIds(ids);
        return  i;
    }

    @Override
    public int materialRemove2(DztMainRoot materialAdd) {
        List<String> collect = materialAdd.getIsoMaterials().stream().map(x -> x.getId()).collect(Collectors.toList());
        int i = materialDaoMapper.deleteBatchIds(collect);
        return i;
    }


}
