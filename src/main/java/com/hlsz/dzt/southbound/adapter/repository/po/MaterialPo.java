package com.hlsz.dzt.southbound.adapter.repository.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hlsz.common.util.BaseEntity;
import lombok.Data;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/7 9:50
 */

@Data
@TableName("PIP_ISO_PPMAT")
public class MaterialPo extends BaseEntity {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    @TableField("MAIN_ID")
    private String mainId;
    @TableField("PARTS_NO")
    private String partsNo;
    @TableField("MATE_CODE")
    private String mateCode;
    @TableField("MATE_CODE_B")
    private String mateCodeB;
    @TableField("SIT_CODE")
    private String sitCode;
    @TableField("UNITS")
    private String units;
    @TableField("ISOMETRIC")
    private String isometric;
    @TableField("SPOOL")
    private String spool;
    @TableField("PIP_LINE_NO")
    private String pipLineNo;
    @TableField("MATE_NAME")
    private String mateName;
    @TableField("GRADE")
    private String grade;
    @TableField("MAT_TYPE")
    private String matType;
    @TableField("MATE_TYPE")
    private String mateType;
    @TableField("SPECS")
    private String specs;
    @TableField("MAT_SIZE")
    private String matSize;
    @TableField("QA_CLASS")
    private String qaClass;
    @TableField("RCCM")
    private String rccm;
    @TableField("PIP_CLS")
    private String pipCls;
    @TableField("MANC_TYPE")
    private String mancType;
    @TableField("PRE_CLS")
    private String preCls;
    @TableField("UNIT_Q")
    private String unitQ;
    @TableField("UNIT_D")
    private String unitD;
    @TableField("LENGTH")
    private String length;
    @TableField("WIDTH")
    private String width;
    @TableField("AMOUNT")
    private String amount;
    @TableField("AMOUNT_D")
    private String amountD;
    @TableField("UNIT_W")
    private String unitW;
    @TableField("WEIGHT")
    private String weight;
    @TableField("WEIGHT_D")
    private String weightD;
    @TableField("PRI_AMOUNT")
    private String priAmount;
    @TableField("FCR_AMOUNT")
    private String fcrAmount;
    @TableField("DIA1")
    private String dia1;
    @TableField("THICK1")
    private String thick1;
    @TableField("WALL_THICK")
    private String wallThick;
    @TableField("DIA2")
    private String dia2;
    @TableField("THICK2")
    private String thick2;
    @TableField("WALL_THICK2")
    private String wallThick2;
    @TableField("MATEMARK")
    private String matemark;
    @TableField("IF_HP")
    private String ifHp;
    @TableField("HP_TYPE")
    private String hpType;
    @TableField("HP_THICKNESS")
    private String hpThickness;
    @TableField("HP_LENGTH")
    private String hpLength;
    @TableField("IF_BR")
    private String ifBr;
    @TableField("PH_LENGTH")
    private String phLength;
    @TableField("EQU_POSI")
    private String equPosi;
    @TableField("ROOM")
    private String room;
    @TableField("MAC_STD")
    private String macStd;
    @TableField("MAC_DRAW")
    private String macDraw;
    @TableField("PRE_WAS")
    private String preWas;
    @TableField("PRE_WAS_DATE")
    private String preWasDate;
    @TableField("WAS_NO")
    private String wasNo;
    @TableField("INS_WAS_DATE")
    private String insWasDate;
    @TableField("IF_PRE")
    private String ifPre;
    @TableField("ONLINE_PARTS")
    private String onlineParts;
    @TableField("DATE_TYPE")
    private String dateType;
    @TableField("REMARK")
    private String remark;
    @TableField("IF_PAINT")
    private String ifPaint;
    @TableField("PAINT_SYS")
    private String paintSys;
    @TableField("PAINT_COL")
    private String paintCol;
    @TableField("PRE_SITE")
    private String preSite;
    @TableField("FACT_MAT")
    private String factMat;
    @TableField("FACT_STE")
    private String factSte;
    @TableField("FACT_SPE")
    private String factSpe;
    @TableField("FACT_LEN")
    private String factLen;
    @TableField("FACT_AMO")
    private String factAmo;
    @TableField("FACT_WEI")
    private String factWei;
    @TableField("BATCH_NO")
    private String batchNo;
    @TableField("STOR_POSI")
    private String storPosi;
    @TableField("REM_LENGTH")
    private String remLength;
    @TableField("REM_AMOUNT")
    private String remAmount;
    @TableField("REM_STOR_POSI")
    private String remStorPosi;
    @TableField("RMS_MAT")
    private String rmsMat;
    @TableField("CONFIRM_EMP")
    private String confirmEmp;
    @TableField("CONFIRM_DATE")
    private String confirmDate;

    @TableField("DELETE_YN")
    private String deleteYn;
    @TableField("DELETE_TIME")
    private String deleteTime;


    @TableField("MRP_NO")
    private String mrpNo;
    @TableField("UNIT_AREA")
    private String unitArea;
    @TableField("MATE_HOLDER")
    private String mateHolder;
    @TableField("DOCID")
    private String docid;
    @TableField("PREFAB_ERECT")
    private String prefabErect;
    @TableField("BEND_RADIUS1")
    private String bendRadius1;
    @TableField("BEND_RADIUS2")
    private String bendRadius2;
    @TableField("BEND_RADIUS3")
    private String bendRadius3;
    @TableField("BEND_RADIUS4")
    private String bendRadius4;
    @TableField("BEND_RADIUS5")
    private String bendRadius5;
    @TableField("BEND_RADIUS6")
    private String bendRadius6;
    @TableField("BEND_RADIUS7")
    private String bendRadius7;
    @TableField("BEND_RADIUS8")
    private String bendRadius8;
    @TableField("BEND_ANGLE1")
    private String bendAngle1;
    @TableField("BEND_ANGLE2")
    private String bendAngle2;
    @TableField("BEND_ANGLE3")
    private String bendAngle3;
    @TableField("BEND_ANGLE4")
    private String bendAngle4;
    @TableField("BEND_ANGLE5")
    private String bendAngle5;
    @TableField("BEND_ANGLE6")
    private String bendAngle6;
    @TableField("BEND_ANGLE7")
    private String bendAngle7;
    @TableField("BEND_ANGLE8")
    private String bendAngle8;
    @TableField("MAT_SIZE1")
    private String matSize1;
    @TableField("MAT_SIZE2")
    private String matSize2;
    @TableField("MAT_SIZE3")
    private String matSize3;
    @TableField("MAT_SIZE4")
    private String matSize4;
    @TableField("MAT_SIZE5")
    private String matSize5;
    @TableField("MAT_SIZE6")
    private String matSize6;
    @TableField("MAT_SIZE7")
    private String matSize7;
    @TableField("MAT_SIZE8")
    private String matSize8;
    @TableField("JOIN_TYPE")
    private String joinType;
    @TableField("WORK_PAGE")
    private String workPage;
    @TableField("S_WORK_PAGE")
    private String sWorkPage;
    @TableField("MAT_TPS")
    private String matTps;
    @TableField("LRCM")
    private String lrcm;
    @TableField("NAMES")
    private String names;
    @TableField("PP_TYPE")
    private String ppType;
    @TableField("FRC")
    private String frc;
    @TableField("COM_UNITS")
    private String comUnits;
    @TableField("DESCES")
    private String desces;
    @TableField("SIZE1")
    private String size1;
    @TableField("SIZE2")
    private String size2;
    @TableField("PRE_LEVEL")
    private String preLevel;
    @TableField("SAFE_CLASS")
    private String safeClass;
    @TableField("STANDS")
    private String stands;
    @TableField("INPUT_EMP")
    private String inputEmp;
    @TableField("INPUT_DATE")
    private String inputDate;
    @TableField("FUNC_NO")
    private String funcNo;
    @TableField("ESP_EQU_TYPE")
    private String espEquType;
    @TableField("MOMENT")
    private String moment;
    @TableField("WAS")
    private String was;
    @TableField("PRES_INFO")
    private String presInfo;
    @TableField("BR_INFO")
    private String brInfo;
    @TableField("PH_LENGHT")
    private String phLenght;
    @TableField("INS_DATE")
    private String insDate;
    @TableField("TFD1")
    private String tfd1;
    @TableField("TFD2")
    private String tfd2;
    @TableField("QUANT_NAME")
    private String quantName;
    @TableField("QUANT_TYPE")
    private String quantType;
    @TableField("QUANT_QTY")
    private String quantQty;
    @TableField("QUANT_UNIT")
    private String quantUnit;
    @TableField("IF_QUANT")
    private String ifQuant;
    @TableField("RVS_COE")
    private String rvsCoe;
    @TableField("SPOOL_ID")
    private String spoolId;
    @TableField("SUP_FUNC")
    private String supFunc;
    @TableField("IF_JOB")
    private String ifJob;
    @TableField("JOB_PLACE")
    private String jobPlace;
    @TableField("BEND_QTY")
    private String bendQty;
    @TableField("OPEN_QTY")
    private String openQty;
    @TableField("FLANGTYPE")
    private String flangtype;
    @TableField("CONFIRM_DATE2")
    private String confirmDate2;
    @TableField("USED_QTY")
    private String usedQty;
    @TableField("OLD_ID")
    private String oldId;
    @TableField("ROWNO")
    private String rowno;
    @TableField("BEND_RADIUS9")
    private String bendRadius9;
    @TableField("BEND_ANGLE9")
    private String bendAngle9;
    @TableField("OPENBOXNO_A")
    private String openboxnoA;
    @TableField("OPENBOXA_DATE_IN")
    private String openboxaDateIn;
    @TableField("OPENBOXNO_B_IN")
    private String openboxnoBIn;
    @TableField("OPENBOXB_DATE_IN")
    private String openboxbDateIn;
    @TableField("PP_DRAW_MATQTY")
    private String ppDrawMatqty;
    @TableField("FIVE_PASS_FLAG")
    private String fivePassFlag;
    @TableField("FIVE_PLAN_PASS")
    private String fivePlanPass;
    @TableField("FIVE_PLAN_DATE")
    private String fivePlanDate;
    @TableField("FIVE_PLAN_MON")
    private String fivePlanMon;
    @TableField("FIVE_PLAN_Y_N")
    private String fivePlanYN;
    @TableField("AMOUNT_FM")
    private String amountFm;
    @TableField("QUANT_QTY_FM")
    private String quantQtyFm;
    @TableField("IS_CANCEL")
    private String isCancel;
    @TableField("FLANGE_JCJLNO")
    private String flangeJcjlno;
    @TableField("VALVE_JCJLNO")
    private String valveJcjlno;
    @TableField("BEND_RADIUS10")
    private String bendRadius10;
    @TableField("BEND_ANGLE10")
    private String bendAngle10;
    @TableField("PRE_WEIGHT_YN")
    private String preWeightYn;
    @TableField("BAIT_COUNT")
    private String baitCount;
    @TableField("MATE_MARK_CODE")
    private String mateMarkCode;
    @TableField("UP_CODE")
    private String upCode;
    @TableField("BAIT_EMP")
    private String baitEmp;
    @TableField("BAIT_DATE")
    private String baitDate;
    @TableField("QC_CONFIRM_EMP")
    private String qcConfirmEmp;
    @TableField("QC_CONFIRM_DATE")
    private String qcConfirmDate;
    @TableField("QUANT_CODE")
    private String quantCode;
    @TableField("PMS_ID")
    private String pmsId;
    @TableField("PMS_VER")
    private String pmsVer;
    @TableField("IF_STANDARDS")
    private String ifStandards;
    @TableField("STANDARDS")
    private String standards;


}
