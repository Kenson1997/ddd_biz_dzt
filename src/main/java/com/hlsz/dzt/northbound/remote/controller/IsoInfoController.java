package com.hlsz.dzt.northbound.remote.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hlsz.common.base.BaseConditionEntity;
import com.hlsz.common.util.JsonResult;
import com.hlsz.common.util.SelectConditionUtil;
import com.hlsz.dzt.domain.dzt.entity.IsoMaterialEntity;
import com.hlsz.dzt.northbound.local.appService.IsoAppService;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoInfoVo;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoMaterialVo;
import com.hlsz.dzt.northbound.remote.message.IsoInfoRequest;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/6 10:35
 */

@RestController
@RequestMapping("/iso")
public class IsoInfoController {

    @Resource
    private IsoAppService isoAppService;  // 等轴图本地服务

/***********************************************************iso 主表信息操作开始**************************************************/
    /**
     * @Description 等轴图新增
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("/add")
    public JsonResult isoAdd(@RequestBody IsoInfoRequest isoInfoRequest){
        boolean b =  isoAppService.add(isoInfoRequest);
        return JsonResult.ok(b ==true?"新增成功！":"新增失败！");
    }

    /**
     * @Description 等轴图更新
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("/update")
    public JsonResult isoUpdate(@RequestBody IsoInfoRequest isoInfoRequest){
        boolean b =  isoAppService.update(isoInfoRequest);
        return JsonResult.ok(b ==true?"修改成功！":"修改失败！");
    }

    /**
     * @Description 等轴图删除
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("/delete/{id}")
    public JsonResult isoDelete(@PathVariable("id") String id){
        boolean b =  isoAppService.delete(id);
        return JsonResult.ok(b ==true?"新增成功！":"新增失败！");
    }

    /**
     * @Description 等轴图列表
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("/list")
    public JsonResult isoList(@RequestBody IsoInfoRequest isoInfoRequest){
      IPage<IsoInfoPo> isoList =  isoAppService.list(isoInfoRequest.getPage(), SelectConditionUtil.parseWhereSql(isoInfoRequest.getConditions()));
        return JsonResult.ok(isoList);
    }

    /**
     * @Description 等轴图审核
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("approve")
    public JsonResult isoApprove(String id){
      boolean flag =   isoAppService.isoApprove(id);
        return JsonResult.ok();
    }

    /**
     * @Description 等轴图升版
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("upVersion")
    public JsonResult upVersion(String id){

        return JsonResult.ok();
    }

/***********************************************************iso 主表信息操作结束**************************************************/


/***********************************************************iso 材料信息操作开始**************************************************/
    /**
     * @Description 等轴图材料新增 （临时保留以VO的形式接收参数）
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("materialAdd")
    public JsonResult materialAdd(@RequestBody IsoMaterialVo isoMaterialVo){
       int a = isoAppService.materialAdd(isoMaterialVo);
        return JsonResult.ok(a);
    }

    /**
     * @Description 等轴图材料更新（临时保留以VO的形式接收参数）
     * @Author kenson
     * @Date 2024/8/6 16:19
     */
    @PostMapping("materialChange")
    public JsonResult materialChange(@RequestBody IsoMaterialVo isoMaterialVo){
        int a = isoAppService.materialChange(isoMaterialVo);
        return JsonResult.ok(a);
    }

    /** 根据等轴图ID 获取等轴图材料*/
    @GetMapping("materialList")
    public JsonResult materialList(Page page, @RequestParam("id") String id){
      IPage<MaterialPo> materialPos =  isoAppService.materialList(page,id);
        return JsonResult.ok(materialPos);
    }

    /**
     * @Description 材料删除方式 一
     * @Author kenson
     * @Date 2024/8/13 10:21
     */
    @DeleteMapping("/materialDelete")
    public JsonResult materialDelete(@RequestParam("ids") List<String> ids){
        int flag =  isoAppService.materialRemove(ids);
        return JsonResult.ok(flag);
    }

    /**
     * @Description 材料删除方式二（临时保留以VO的形式接收参数）
     * @Author kenson
     * @Date 2024/8/13 10:21
     */
    @DeleteMapping("/materialDelete2")
    public JsonResult materialDelete2(@RequestBody IsoMaterialVo isoMaterialVo){
      int i  =  isoAppService.materialRemove2(isoMaterialVo);
        return JsonResult.ok(i);
    }

/***********************************************************iso 材料信息操作结束**************************************************/






}



