package com.hlsz.dzt.northbound.remote.message;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hlsz.common.base.BaseConditionEntity;
import com.hlsz.dzt.domain.dzt.DztMainRoot;
import com.hlsz.dzt.domain.dzt.entity.IsoMaterialEntity;
import com.hlsz.dzt.message.converter.DztpwCalculateAssembler;
import com.hlsz.dzt.message.converter.ProgressAssembler;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoInfoVo;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoMaterialVo;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoPipsVo;
import com.hlsz.dzt.southbound.adapter.repository.IsoInfoRepositoryImpl;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import com.hlsz.dzt.southbound.port.repository.IsoInfoRepository;
import lombok.Data;

import java.util.List;

/**
 * @Description 等轴图消息契约
 * @Author kenson
 * @Date 2024/8/6 14:11
 */

@Data
public class IsoInfoRequest {

    private String   confirmEmp;    //  审核人
    private String   confirmDate;    //  审核日期
    private String   id;    //  主键
    private String   uniqueCode;    //  唯一标识
    private String   isometric;    //  等轴图号
    private String   isoDrawNo;    //  原图编码
    private String   docid;    //  图纸编号
    private String   volLabel;    //  卷标
    private String   pages;    //  页码
    private String   mapsheet;    //  图幅
    private String   drawStatus;    //  图纸状态
    private String   drawType;    //  图纸类型
    private String   ver;    //  版本
    private String   newVer;    //  最新版
    private String   units;    //  机组
    private String   systems;    //  系统
    private String   subsystems;    //  子系统
    private String   building;    //  厂房
    private String   area;    //  区域
    private String   room;    //  房间
    private String   clevel;    //  标高
    private String   cleve2;    //  顶标高
    private String   diameter;    //  直径
    private String   diaType;    //  直径类型
    private String   grade;    //  钢号
    private String   matType;    //  材质
    private String   rccm;    //  RCCM级别
    private String   pipeGrade;    //  管道等级
    private String   sysMedia;    //  介质
    private String   pipCls;    //  管道级别
    private String   qaClass;    //  质保级别
    private String   dustClass;    //  清洁度级别
    private String   bendStd;    //  弯管标准
    private String   safeCls;    //  安全等级
    private String   earthquake;    //  抗震类别
    private String   compuNo;    //  应力计算书号
    private String   ifHp;    //  是否保温
    private String   hpType;    //  保温类型
    private String   hpLength;    //  保温长度
    private String   hpThickness;    //  保温厚度
    private String   workH;    //  工作温度(℃)
    private String   workP;    //  工作压力(MPa)
    private String   ifPaint;    //  是否油漆
    private String   paintSys;    //  涂层系统
    private String   paintCol;    //  油漆颜色
    private String   ifBr;    //  是否伴热
    private String   phLength;    //  伴热长度
    private String   preLength;    //  预制长度(mm)
    private String   insLength;    //  安装长度(mm)
    private String   prjLength;    //  工程长度(mm)
    private String   pointType;    //  点数计算分类
    private String   pipeWeight;    //  管道重量(kg)
    private String   equiWeight;    //  设备重量(kg)
    private String   tWeight;    //  总重量(kg)
    private String   tDots;    //  总点数
    private String   preWeight;    //  预制重量(kg)
    private String   preDots;    //  预制点数
    private String   insWeight;    //  安装重量(kg)
    private String   insDots;    //  安装点数
    private String   desComp;    //  设计公司
    private String   preComp;    //  预制公司
    private String   insComp;    //  安装公司
    private String   buildDrawing;    //  竣工图
    private String   bulidIssueDate;    //  竣工图发布日期
    private String   upDrawCode;    //  上游图号
    private String   downDrawCode;    //  下游图号
    private String   verModiCode;    //  版本修订码
    private String   ifFu;    //  FORUSE
    private String   letterNo;    //  FORUSE信函
    private String   preLetterNo;    //  预制信函号
    private String   drawCancel;    //  图纸取消
    private String   drawCancelNotes;    //  取消原因
    private String   fileDistrib;    //  文件分发单
    private String   delIdcDate;    //  移交IDC
    private String   distDate;    //  分发日期
    private String   executeDesc;    //  执行状态描述
    private String   pactNo;    //  合同号
    private String   paintArea;    //  油漆总面积
    private String   insDrawFlag;    //  安装图标记
    private String   docCode;    //  存档号
    private String   typicalDraw;    //  典型（详）图
    private String   typicalDrawVer;    //  典型（详）图版本
    private String   sitCode;    //  部件位号
    private String   ifPre;    //  是否预制
    private String   ifConversed;    //  是否转化
    private String   ifConversedFinish;    //  已转化
    private String   ifYumai;    //  明装/预埋
    private String   empPre;    //  预制工作包
    private String   empIns;    //  安装工作包
    private String   statusDate;    //  状态时间
    private String   issuePre;    //  预制发布
    private String   issueIns;    //  安装发布
    private String   wbsCodePre;    //  预制wbs
    private String   wbsCodeIns;    //  安装wbs
    private String   projCode;    //  项目代码
    private String   engineeringCode;    //  工程代码
    private String   reactortype;    //  反应堆类型
    private String   deleteYn;    //  删除标记
    private String   deleteTime;    //  删除日期
    private String   foundMan;    //  创建人
    private String   foundDate;    //  创建日期
    private String   modifMan;    //  修改人
    private String   lastModifDate;    //  修改日期
    private String   compCode;    //  公司代码
    private String   deptCode;    //  部门代码
    private String   orgFullcode;    //  组织机构全码
    private String   orgId;    //  组织机构id
    private String   ffArea;    //  防腐总面积
    private String   fuReceiver;    //  FORUSE信函接收日期
    private String   module;    //  模块
    private String   penetrate;    //  贯穿件
    private String   drawIncmanPre;    //  预制图接收人
    private String   drawIncdatePre;    //  预制图接收日期
    private String   wfPre;    //  预制工作文件
    private String   wfVerPre;    //  预制工作文件版本
    private String   wfIncdatePre;    //  预制工作文件接收日期
    private String   wfCancelPre;    //  预制工作文件取消
    private String   wfCcdatePre;    //  预制工作文件取消日期
    private String   wfCsdatePre;    //  预制工作文件关闭日期
    private String   issueByPre;    //  预制发布人
    private String   issueDatePre;    //  预制发布日期
    private String   isuseVerPre;    //  预制发布版本
    private String   isuseStatusPre;    //  预制发布状态
    private String   tqpCodePre;    //  预制质量文件号
    private String   wasReceiver;    //  was接收日期
    private String   isMat;    //  材料已接收
    private String   isSend;    //  材料已下发
    private String   sendDept;    //  发放单位
    private String   sendDate;    //  发放时间
    private String   issDate;    //  预制指令发布
    private String   matIss;    //  下料单发布日期
    private String   preComDate;    //  预制完成日期
    private String   rtDate;    //  rt完成日期
    private String   closeBatch;    //  关批
    private String   batchDate;    //  炉批号输入日期
    private String   paintDate;    //  油漆完成日期
    private String   xdDate;    //  消点日期
    private String   qcDate;    //  qc放行日期
    private String   preEmrDate;    //  质量文件关闭日期
    private String   instoreDate;    //  入库日期
    private String   storeArea;    //  存储区
    private String   faceStore;    //  室内存储区域
    private String   outstoreDate;    //  出库日期
    private String   moveMan;    //  移交人
    private String   carNo;    //  车号
    private String   insCarNo;    //  试压回路/装车单号
    private String   drawIncmanIns;    //  安装图接收人
    private String   drawIncdateIns;    //  安装图接收日期
    private String   wfIns;    //  安装工作文件
    private String   wfVerIns;    //  安装工作文件版本
    private String   issueByIns;    //  安装发布人
    private String   issueDateIns;    //  安装发布日期
    private String   isuseVerIns;    //  安装发布版本
    private String   isuseStatusIns;    //  安装发布状态
    private String   tqpCodeIns;    //  安装质量文件号
    private String   ptConsign;    //  油漆委托
    private String   ptConsignCode;    //  委托单号
    private String   ptConsignEmp;    //  委托人
    private String   ptConsignDate;    //  委托日期
    private String   tmCheck;    //  油漆分派
    private String   tmCheckCode;    //  分派单号
    private String   tmCheckEmp;    //  分派人
    private String   tmCheckDate;    //  分派日期
    private String   imHd;    //  油漆移交
    private String   imHdCode;    //  移交单号
    private String   imHdEmp;    //  移交人
    private String   imHdDate;    //  移交日期
    private String   paintType;    //  油漆类型
    private String   slCode;    //  申领单号
    private String   slMan;    //  申领人
    private String   slDate;    //  申领日期
    private String   ckCode;    //  出库单号
    private String   ckMan;    //  出库人
    private String   ckDate;    //  出库日期
    private String   instor;    //  入库
    private String   instorEmp;    //  入库人
    private String   storeDate;    //  入库日期
    private String   empNo;    //  工作包
    private String   dia;    //  主管直径
    private String   syphonStd;    //  弯管标准
    private String   keeparch;    //  存档号
    private String   wjlx;    //  图纸种类
    private String   dispenseDate;    //  分发日期
    private String   incept;    //  接收人
    private String   letterDate;    //  接收日期
    private String   wf;    //  工作文件（预制）
    private String   wfVer;    //  工作文件版本（预制）
    private String   wfCodeIns;    //  工作文件（安装）
    private String   spooline;    //  管线号
    private String   drawNo;    //  图册号
    private String   openitemNo;    //  开口项编号
    private String   operP;    //  运行压力
    private String   floors;    //  楼层
    private String   remarks;    //  备注
    private String   gfdj;    //  规范等级-管线
    private String   motionH;    //  运行温度
    private String   preLetterDate;    //  预制信函时间
    private String   docName;    //  中文名称
    private String   ptDate;    //  PT日期
    private String   ptCloseBatch;    //  PT关批
    private String   intercode;    //  内部文件编号
    private String   recCode;    //  收文号
    private String   rccm2;    //  rccm级别2
    private String   vfpBakBz;    //  VFP图纸备份
    private String   drawNoVer;    //  图册版本
    private String   drawNoName;    //  图册名称
    private String   epcBuilding;    //  EPC厂房
    private String   epcClevel;    //  EPC标高
    private String   specialFf;    //  特殊防腐
    private String   ifOutdoor;    //  是否室外

    private List<BaseConditionEntity>  conditions ;  // 查询条件

    private Page page;   // 分页

    // 调用重量计算装配器， 完成重量计算
    private DztpwCalculateAssembler dztpwCalculateAssembler = SpringUtil.getBean(DztpwCalculateAssembler.class);
    // 调用进度指示书装配器， 获取进度指示书中点数信息
    private ProgressAssembler progressAssembler = SpringUtil.getBean(ProgressAssembler.class);
    //  调用南向网关， 用于初始化聚合
    private IsoInfoRepository isoInfoRepository = SpringUtil.getBean(IsoInfoRepositoryImpl.class);



    public static IsoInfoRequest  init(){
        IsoInfoRequest isoInfoRequest = new IsoInfoRequest();
        return isoInfoRequest;
    }
    public static IsoInfoRequest  init(String id){
        IsoInfoRequest isoInfoRequest = new IsoInfoRequest();
        isoInfoRequest.setId(id);
        return isoInfoRequest;
    }


    /**  根据vo对象，将VO 对象初始化聚合,用于页面新增操作*/
    public DztMainRoot toDztMainRootForAdd(){
        DztMainRoot dztMainRoot = new DztMainRoot();
        return dztMainRoot.initDztBasicInfo(this).initDztDescEntity(this);
    }

    public DztMainRoot toDztMainRootForUpdate(){
        DztMainRoot dztMainRoot = new DztMainRoot();
        IsoInfoPo isoInfoPo = isoInfoRepository.getById(this.id);
        BeanUtil.copyProperties(this,isoInfoPo);
        return dztMainRoot.initDztBasicInfo(isoInfoPo).initDztDescEntity(isoInfoPo);
    }


    /** 根据 数据id 初始化聚合，用于图纸审核操作 */
    public DztMainRoot toDztMainRootForApprove(){
        DztMainRoot dztMainRoot = new DztMainRoot();
        IsoInfoPo isoInfoPo = isoInfoRepository.getById(id);
        //  此处调用点数计算的服务， 根据等轴图的id 将整个图的点数计算出来
        return  dztMainRoot.initDztpwEntity(isoInfoPo);
    }


    /**  批量新增管道数据*/
    public DztMainRoot toDztMainRootForPipInfosManager(IsoPipsVo isoPipsVo){
        DztMainRoot dztMainRoot = new DztMainRoot();
        dztMainRoot.setPipInfos(isoPipsVo.getPipinfos());
        return dztMainRoot;
    }


    public DztMainRoot toMaterialAdd(IsoMaterialVo isoMaterialVo) {
        DztMainRoot dztMainRoot = new DztMainRoot();
        List<IsoMaterialEntity> isoMaterialEntities = BeanUtil.copyToList(isoMaterialVo.getMaterials(), IsoMaterialEntity.class);
        dztMainRoot.setIsoMaterials(isoMaterialEntities);
        return dztMainRoot;
    }

    public DztMainRoot toMaterialChange(IsoMaterialVo isoMaterialVo) {
        DztMainRoot dztMainRoot = new DztMainRoot();
         dztMainRoot = dztMainRoot.initIsoMaterialList(isoMaterialVo.getMaterials());
//        for(Object obj: isoMaterialVo.getMaterials()){
//            IsoMaterialEntity isoMaterialEntity = new IsoMaterialEntity();
//            BeanUtil.copyProperties(obj,isoMaterialEntity);
//            isoMaterials.add(isoMaterialEntity);
//        }
        return dztMainRoot;
    }
}
