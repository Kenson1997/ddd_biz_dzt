package com.hlsz.dzt.northbound.remote.controller.vo;

import com.hlsz.dzt.domain.dzt.entity.PipInfoEntity;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/6 18:25
 */

@Data
public class IsoPipsVo {

    private String id;

    private List<PipInfoEntity> pipinfos;


}




