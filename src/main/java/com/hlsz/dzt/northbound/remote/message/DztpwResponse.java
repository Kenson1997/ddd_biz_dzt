package com.hlsz.dzt.northbound.remote.message;

import com.hlsz.dzt.domain.dzt.DztMainRoot;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description 点数计算响应对象
 * @Author kenson
 * @Date 2024/8/6 14:39
 */

@Data
public class DztpwResponse {
    private String id ;
    private BigDecimal pipeWeight;   // 管道重量(kg)
    private BigDecimal equiWeight;   // 设备重量(kg)
    private BigDecimal tWeight;   // 总重量(kg)
    private BigDecimal tDots;   // 总点数
    private BigDecimal preWeight;   // 预制重量(kg)
    private BigDecimal preDots;   // 预制点数
    private BigDecimal insWeight;   // 安装重量(kg)
    private BigDecimal insDots;   // 安装点数



    public static DztpwResponse init(String id){
        DztpwResponse dztpwResponse = new DztpwResponse();
        dztpwResponse.setId(id);
        return dztpwResponse;
    }

    public DztMainRoot toDztMainRoot(){

        return null;
    }

//    public IsoInfoVo to(){
//        IsoInfoVo isoInfoVo = new IsoInfoVo();
//
//    }





}
