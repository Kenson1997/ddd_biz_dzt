package com.hlsz.dzt.northbound.remote.message;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hlsz.common.base.BaseConditionEntity;
import lombok.Data;

import java.util.List;

/**
 * @Description 焊口 消息契约
 * @Author kenson
 * @Date 2024/8/13 15:21
 */

@Data
public class PipWeldRequest {
    private String id; //主键
    private String  mainId; //外键
    private String confirmEmp; //审核人
    private String  confirmDate; //审核日期
    private String room; //房间
    private String  systems; //系统
    private String subsystems; //子系统
    private String  leveles; //标高
    private String serial; //制造号
    private String  assembly; //焊缝号/接口标识
    private String newVer; //最新版
    private String  validYn; //有效
    private String bendNum; //弯曲数量
    private String  weldType; //焊接类型
    private String sequence; //标准工序
    private String  jointType; //焊接方式
    private String rccm; //规范等级
    private String  mateGroup; //材料组别
    private String pipCls; //管道级别
    private String  preCls; //压力级别
    private String groupCode; //组别号
    private String  safeCls; //安全级别
    private String grade; //钢号
    private String  matType; //材质
    private String diameter; //直径
    private String  thickness; //厚度
    private String weldInch; //焊寸
    private String  tempLink; //临时连接
    private String jointReq; //连接技术要求
    private String  chkDta; //验收标准
    private String matTypeUp; //上游物项类型
    private String  empUp; //上游工作包
    private String lineNoUp; //上游管线
    private String  upUnits; //上游机组
    private String upSpool; //上游等轴图/设备
    private String  spoolUp; //上游管段号
    private String lrcmUp; //上游物项代码
    private String  rccmUp; //上游RCCM
    private String diamUp; //上游直径
    private String  thickUp; //上游厚度
    private String thickSerUp; //上游厚度系列
    private String  wedPatUp; //上游连接部件号
    private String steelUp; //上游钢号
    private String  materialUp; //上游材质
    private String blDrawUp; //上游所在图
    private String  pipeClass1; //上游管道级别
    private String empDown; //下游工作包
    private String  lineNoDown; //下游管线号
    private String dnUnits; //下游机组
    private String  dnSpool; //下游等轴图/设备
    private String spoolDown; //下游管段
    private String  matTypeDown; //下游物项类型
    private String lrcmDown; //下游物项代码
    private String  rccmDown; //下游RCCM
    private String diamDown; //下游直径
    private String  thickDown; //下游厚度
    private String thickSerDown; //下游厚度系数
    private String  steelDown; //下游钢号
    private String materialDown; //下游材质
    private String  wedPatDown; //下游连接部件号
    private String blDrawDown; //下游所在图
    private String  pipeClass2; //下游管道级别
    private String rms; //保留项
    private String  serveChk; //在役检查
    private String adjustYn; //调节口
    private String  weldingLine; //丁字焊缝
    private String difficultyFlag; //困难焊口
    private String  difficultyDate; //困难口标注日期
    private String projCode; //项目代码
    private String  engineeringCode; //工程代码
    private String reactortype; //反应堆类型
    private String  deleteYn; //删除标记
    private String deleteTime; //删除日期
    private String  foundMan; //创建人
    private String foundDate; //创建日期
    private String  modifMan; //修改人
    private String lastModifDate; //修改日期
    private String  compCode; //公司代码
    private String deptCode; //部门代码
    private String  orgFullcode; //组织机构全码
    private String orgId; //组织机构id
    private String  pipeLineNo; //管线号
    private String rtSamplingNo; //不参与RT抽批
    private String  building; //厂房
    private String pipeIso; //等轴图号
    private String  prefabErect; //焊缝类型
    private String serveWeldMethods; //在役焊缝检验方法
    private String  carateWho; //输入人
    private String carateDate; //输入日期
    private String  rms2; //保留类型
    private String specUp; //规格（上游）
    private String  specDown; //规格（下游）
    private String zyqChk; //是否役前检查
    private String  preSite; //预制地点
    private String matenameUp; //上游物项名称
    private String  matenameDown; //下游物项名称
    private String thickness2; //厚度2
    private String  ifWk; //是否温控
    private String ptSamplingNo; //不参与PT分批
    private String  allPt; //100%PT
    private String allRt; //100%RT
    private String  ifYumai; //是否预埋
    private String formTiein; //坡口形式
    private String  ifBoss; //是否BOSS头
    private String wedProcess; //焊接工艺
    private String  ifJxh; //机械化焊
    private String ifRcl; //热处理
    private String  ifHskl; //焊丝控Cr


    private List<BaseConditionEntity> conditions ;  // 查询条件

    private Page page;   // 分页

    public static PipWeldRequest  init(){
        PipWeldRequest pipWeldRequest = new PipWeldRequest();
        return pipWeldRequest;
    }

}
