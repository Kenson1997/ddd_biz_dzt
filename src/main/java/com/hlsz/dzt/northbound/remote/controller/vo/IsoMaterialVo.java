package com.hlsz.dzt.northbound.remote.controller.vo;

import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/7 9:49
 */

@Data
public class IsoMaterialVo {

    private String id;
    private List<Object> materials;

}



