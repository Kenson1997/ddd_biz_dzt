package com.hlsz.dzt.northbound.local.appService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hlsz.dzt.domain.domain_service.IsoDomainService;
import com.hlsz.dzt.domain.dzt.DztMainRoot;
import com.hlsz.dzt.domain.dzt.entity.IsoMaterialEntity;
import com.hlsz.dzt.northbound.remote.message.IsoInfoRequest;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoInfoVo;
import com.hlsz.dzt.northbound.remote.controller.vo.IsoMaterialVo;
import com.hlsz.dzt.southbound.adapter.repository.po.IsoInfoPo;
import com.hlsz.dzt.southbound.adapter.repository.po.MaterialPo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description TODO
 * @Author kenson
 * @Date 2024/8/6 18:32
 */

@Service
public class IsoAppService {

    @Resource
    private IsoDomainService isoDomainService;  // 等轴图领域服务

    public boolean add(IsoInfoRequest isoInfoRequest) {
        DztMainRoot dztMainRootForAdd =isoInfoRequest.toDztMainRootForAdd();
        boolean b = isoDomainService.save(dztMainRootForAdd);
        return b;
    }

    public int materialAdd(IsoMaterialVo isoMaterialVo) {
        DztMainRoot materialAdd  = IsoInfoRequest.init(isoMaterialVo.getId()).toMaterialAdd(isoMaterialVo);
        int i =  isoDomainService.isoAddMaterials(materialAdd);
        return i;
    }

    public int materialChange(IsoMaterialVo isoMaterialVo) {
        DztMainRoot materialChange  = IsoInfoRequest.init(isoMaterialVo.getId()).toMaterialChange(isoMaterialVo);
        int i = isoDomainService.isoChangeMaterials(materialChange);
        return i;
    }

    public boolean update(IsoInfoRequest isoInfoRequest) {
        DztMainRoot dztMainRootForUpdate = isoInfoRequest.toDztMainRootForUpdate();
        boolean b = isoDomainService.update(dztMainRootForUpdate);
        return b;
    }

    public boolean delete(String id) {
        boolean flag =  isoDomainService.delete(id);
        return flag;
    }

    public IPage<IsoInfoPo> list(Page page, QueryWrapper queryWrapper) {
        IPage<IsoInfoPo> lists = isoDomainService.pageList(page,queryWrapper);
        return lists;
    }

    public boolean isoApprove(String id) {
       boolean flag =  isoDomainService.isoApprove(id);
        return flag;
    }

    public IPage<MaterialPo> materialList(Page page, String id) {
      IPage<MaterialPo> listPage =  isoDomainService.materialList(page,id);
        return listPage;
    }

    public int materialRemove(List<String> ids) {
        int flag =isoDomainService.materialRemove(ids);
        return flag;
    }

    public int materialRemove2(IsoMaterialVo isoMaterialVo) {
        DztMainRoot materialAdd = IsoInfoRequest.init(isoMaterialVo.getId()).toMaterialAdd(isoMaterialVo);
        int i = isoDomainService.materialRemove2(materialAdd);
        return i;
    }
}




